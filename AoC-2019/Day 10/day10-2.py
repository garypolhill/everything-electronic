#!/usr/bin/env python3

import sys

class Map:

    def __init__(self, mappachr):
        self.mappa = {}

        y = 0
        for rowstr in mappachr:
            x = 0
            for c in rowstr:
                if c == "#":
                    self.mappa[x, y] = 1
                x += 1
            y += 1

    def detectables(self, x, y):
        if not (x, y) in self.mappa:
            return []

        detect_list = []
        for (x1, y1) in self.mappa:
            if x1 == x and y1 == y:
                continue

            detectable = True

            dx1 = x1 - x
            dy1 = y1 - y
                
            for (x2, y2) in self.mappa:
                if (x2 == x and y2 == y) or (x2 == x1 and y2 == y1):
                    continue

                # To obstruct the view from (x, y) to (x1, y1), (x2, y2) needs
                # to be between them. For that to be true, dx1 * dx2 and
                # dy1 * dy2 must both be positive, and abs(dx2) < abs(dx1) and
                # abs(dy2) < abs(dy1)

                dx2 = x2 - x
                dy2 = y2 - y

                if dx1 * dx2 < 0 or dy1 * dy2 < 0:
                    continue

                if abs(dx2) > abs(dx1) or abs(dy2) > abs(dy1):
                    continue

                # (x, y), (x1, y1) and (x2, y2) are collinear if they
                # form a triangle with area zero

                # this is given by 0.5 * det A, where
                #
                #     | ( x - x1)   (x1 - x2) |
                # A = |                       |
                #     | ( y - y1)   (y1 - y2) |
                #
                # See https://www.urbanpro.com/gre/
                #    how-to-determine-if-points-are-collinear

                # Since the only way the area will be zero is if det A is
                # zero, we just need to compute det A, which, in general, is:
                #
                #     | a   b |
                # det |       | = ad - cb
                #     | c   d |
                #
                # This is zero iff ad == cb
                #
                # See https://www.purplemath.com/modules/determs.htm
                
                if ((x - x1) * (y1 - y2)) == ((y - y1) * (x1 - x2)):
                    detectable = False
                    break
                

            if detectable:
                detect_list.append((x1, y1))
                
        return detect_list

    def detections(self, x, y):
        return(len(self.detectables(x, y)))

    def bestLocation(self):
        maxdetect = -1
        maxx = -1
        maxy = -1
        
        for (x, y) in self.mappa:
            dt = self.detections(x, y)
            if dt > maxdetect:
                maxdetect = dt
                maxx = x
                maxy = y

        return (maxx, maxy, maxdetect)

    def vaporizationOrder(self, x, y):
        

def test(id, arr, dt, dx, dy):
    m = Map(arr)

    (x, y, t) = m.bestLocation()

    if t == dt and x == dx and y == dy:
        print("Test", id, " -- passed")
        return True
    else:
        print("Test", id, " -- FAILED")
        print("\tGot     : (", x,  ",",  y, ") with",  t, "detections")
        print("\tExpected: (", dx, ",", dy, ") with", dt, "detections")
        return False

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    tests = [ [ [".#..#", ".....", "#####", "....#", "...##"], 3, 4, 8],
        [ ["......#.#.", "#..#.#....", "..#######.", ".#.#.###..", ".#..#.....",
          "..#....#.#", "#..#....#.", ".##.#..###", "##...#..#.", ".#....####"],
            5, 8, 33],
        [ ["#.#...#.#.", ".###....#.", ".#....#...", "##.#.#.#.#", "....#.#.#.",
          ".##..###.#", "..#...##..", "..##....##", "......#...", ".####.###."],
            1, 2, 35],
        [ [".#..#..###", "####.###.#", "....###.#.", "..###.##.#", "##.##.#.#.",
          "....###..#", "..#.#..#.#", "#..#.#.###", ".##...##.#", ".....#.#.."],
            6, 3, 41],
        [ [".#..##.###...#######",
           "##.############..##.",
           ".#.######.########.#",
           ".###.#######.####.#.",
           "#####.##.#.##.###.##",
           "..#####..#.#########",
           "####################",
           "#.####....###.#.#.##",
           "##.#################",
           "#####.##.###..####..",
           "..######..##.#######",
           "####.##.####...##..#",
           ".#####..#.######.###",
           "##...#.##########...",
           "#.##########.#######",
           ".####.#.###.###.#.##",
           "....##.##.###..#####",
           ".#.#.###########.###",
           "#.#.#.#####.####.###",
           "###.##.####.##.#..##"], 11, 13, 210] ]

    ok = True
    for i in range(len(tests)):
        ok = test(i + 1, tests[i][0], tests[i][3], tests[i][1], tests[i][2]) \
          and ok

    if ok:
        lines = []
        with open(input_file) as f:
            for line in f:
                lines.append(line)
        
        m = Map(lines)
        (bx, by, bd) = m.bestLocation()
        print("Best is (", bx, ", ", by, ") with", bd,
                  "other asteroids detected")
    else:
        print("Tests failed. Not running main puzzle")

    sys.exit(1)

if __name__ == "__main__":
    main(sys.argv)
