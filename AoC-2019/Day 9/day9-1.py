#!/usr/bin/env python3

import csv, sys

class IntCode:

    # Arguments for instructions. 0 = read, 1 = write
    instargs = [[0, 0, 1],                # Add
                [0, 0, 1],                # Multiply
                [1],                      # Input
                [0],                      # Output
                [0, 0],                   # Jump if true
                [0, 0],                   # Jump if false
                [0, 0, 1],                # Less than
                [0, 0, 1],                # Equals
                [0]]                      # Adjust relative base

    def __init__(self, program, prog_input = []):
        self.program = {}
        self.initial = {}
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        i = 0
        for instruction in program:
            try:
                self.program[i] = int(instruction)
                self.initial[i] = int(instruction)
            except ValueError:
                print("Ignoring non-integer instruction", instruction)
            i += 1
        self.PC = 0
        self.RC = 0                       # Relative address position pointer
        self.input_read = False
        self.awaiting_input = False
        self.output_written = False
        self.error_condition = False
        self.status_message = "OK"

    def copy(self):
        return IntCode(self.program, self.input)

    def reset(self, prog_input = []):
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        self.PC = 0
        self.RC = 0
        self.input_read = False
        self.awaiting_input = False
        self.output_written = False
        self.error_condition = False
        self.status_message = "OK"
        self.program = {}
        for i in self.initial:
            self.program[i] = self.initial[i]

    def inputRead(self, clear = True):
        ir = self.input_read
        if clear: self.input_read = False
        return ir

    def outputWritten(self, clear = True):
        ow = self.output_written
        if clear: self.output_written = False
        return ow

    def awaitingInput(self, clear = False):
        ai = self.awaiting_input
        if clear: self.awaiting_input = False
        return ai

    def errorCondition(self):
        return self.error_condition

    def statusMessage(self):
        return self.status_message

    def addInput(self, prog_input):
        self.input.append(prog_input)

    def getOutput(self):
        return [self.output[i] for i in range(len(self.output))]

    def getLastOutput(self, clear = True):
        lo = self.output[-1]
        if clear: self.output = []
        return lo

    def state(self):
        return {self.program[i] for i in self.program}

    def get(self, loc):
        if loc == None or loc < 0:
            self.error_condition = True
            if loc != None:
                self.status_message = \
                  "Segment violation reading from {}".format(loc)
            return None
        if not loc in self.program:
            self.program[loc] = 0
        return self.program[loc]

    def put(self, loc, value):
        if loc == None or loc < 0:
            self.error_condition = True
            if loc != None:
                self.status_message = \
                  "Segment violation writing to {}".format(loc)
            return None
        try:
            self.program[loc] = int(value)
        except ValueError:
            self.error_condition = True
            self.status_message = \
              "Illegal value to write to {0}: {1}".format(loc, str(value))
        
    def step(self):
        if self.error_condition:
            print(self.status_message)
            return
        
        opcode = self.get(self.PC)

        i = opcode % 100
        
        n = 0
        params = []
        opargs = list(str(opcode // 100))
        opargs.reverse()

        if i >= 1 and i <= len(IntCode.instargs):
            n = len(IntCode.instargs[i - 1])

        self.PC += 1

        for j in range(n):
            param = self.get(self.PC + j)
            if j >= len(opargs) or opargs[j] == '0':
                if IntCode.instargs[i - 1][j] == 0:
                    param = self.get(param)
            elif j < len(opargs) and opargs[j] == '2':
                if IntCode.instargs[i - 1][j] == 0:
                    param = self.get(self.RC + param)
                else:
                    param = self.RC + param
            elif j < len(opargs) and opargs[j] != '1':
                self.error_condition = True
                self.status_message = \
                  "Illegal opargs {0} from opcode {1}".format(opargs, opcode)
                return False
            params.append(param)

        self.PC += n
        
        # Addition: 
        if i == 1:
            self.put(params[2], params[0] + params[1])

        # Multiplication: 
        elif i == 2:
            self.put(params[2], params[0] * params[1])

        # Input
        elif i == 3:
            if(len(self.input) == 0):
                self.awaiting_input = True
                self.PC -= n + 1
                return False
            else:
                self.awaiting_input = False
                self.put(params[0], self.input.pop(0))
                self.input_read = True

        # Output
        elif i == 4:
            self.output.append(params[0])
            self.output_written = True

        # Jump-If-True
        elif i == 5:
            if params[0] != 0:
                self.PC = params[1]

        # Jump-If-False
        elif i == 6:
            if params[0] == 0:
                self.PC = params[1]

        # Less Than
        elif i == 7:
            if params[0] < params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Equals
        elif i == 8:
            if params[0] == params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Adjust relative base
        elif i == 9:
            self.RC += params[0]
            
        # Termination
        elif i == 99:
            self.PC -= 1                  # If step called again, terminate
            print("Termination instruction reached")
            return False

        # Unrecognised instruction
        else:
            self.error_condition = True
            self.status_message = \
              "Illegal instruction {0} from opcode {1}".format(i, opcode)
            return False
        
        return True

    def terminated(self):
        return (self.error_condition or self.get(self.PC) == 99)
            
    def run(self, maxstep = 0):
        nstep = 0
        while not self.terminated():
            self.step()
            nstep += 1
            if maxstep > 0 and nstep >= maxstep:
                print("Step limit", maxstep, "breached")
                break
        return not self.error_condition
        
    @staticmethod
    def readPrograms(file_name):
        programs = []
        with open(file_name, newline = '') as f:
            for row in csv.reader(f):
                programs.append(IntCode(row))
        return programs

def test(id, program, data, expected):
    prog = IntCode(program, data)
    prog.run()
    output = prog.getOutput()
    if prog.errorCondition():
        print("Test", id, "-- error condition detected:", prog.statusMessage())
    if(output == expected):
        print("Test", id, "-- pass")
        return True
    else:
        print("Test", id, "-- FAIL")
        for i in range(len(output)):
            if(i < len(expected) and output[i] != expected[i]):
                print("\tAt", i, "output is", output[i], "but", expected[i],
                          "expected")
        return False


def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.csv"
    programs = IntCode.readPrograms(input_file)
    print("Read", len(programs), "programs from", input_file)

    # Tests

    tests = [([109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99],
              [],
              [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]),
             ([1102,34915192,34915192,7,4,7,99,0],
              [],
              [1219070632396864]),
             ([104,1125899906842624,99],
              [],
              [1125899906842624])]

    ok = True
    i = 0
    for t in tests:
        i += 1
        ok = test(i, t[0], t[1], t[2]) and ok
    
    programs[0].addInput(1)
    programs[0].run()
    print("Output of BOOST program for part 1", programs[0].getOutput())

    programs[0].reset([2])
    programs[0].run()
    print("Output of BOOST program for part 2", programs[0].getOutput())
        
    sys.exit(0)

if __name__ == "__main__":
    main(sys.argv)
