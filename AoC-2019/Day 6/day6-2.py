#!/usr/bin/env python3

import sys, io

class Tree:

    def __init__(self, linkstrs, sep = ')', rev = False):
        self.nodes = {}                   # Node name -> number of links from it
        self.links = {}
        self.tlinks = {}
        self.n_links = 0
        self.n_tlinks = 0

        for link in linkstrs:
            nodestrs = link.split(sep)
            if rev:
                nodestrs.reverse()

            for i in range(len(nodestrs) - 1):
                if not nodestrs[i] in self.nodes \
                  or self.nodes[nodestrs[i]] == None:
                    self.nodes[nodestrs[i]] = nodestrs[i + 1]
                elif nodestrs[i] in self.nodes \
                  and self.nodes[notestrs[i]] != self.nodes[nodestrs[i + 1]]:
                    print(nodestrs[i], "already in orbit around",
                              self.nodes[nodestrs[i]], "and can't also be",
                              "in orbit around", nodestrs[i + 1],
                              file = sys.stderr)
                    sys.exit(1)
                    
                if not nodestrs[i + 1] in self.nodes:
                    self.nodes[nodestrs[i + 1]] = None

                if not nodestrs[i + 1] in self.links:
                    self.links[nodestrs[i + 1]] = {}

                self.links[nodestrs[i + 1]][nodestrs[i]] = 1
                self.n_links += 1

        for node1 in self.nodes:
            self.tlinks[node1] = {}

            if node1 in self.links:
                for node2 in self.links[node1]:
                    self.tlinks[node1][node2] = 1
                    self.n_tlinks += 1
                    
                    if node2 in self.tlinks:
                        for node3 in self.tlinks[node2]:
                            self.tlinks[node1][node3] = 1
                            self.n_tlinks += 1

                parent = self.nodes[node1]
                while parent in self.tlinks:
                    for node3 in self.tlinks[node1]:
                        if not node3 in self.tlinks[parent]:
                            self.tlinks[parent][node3] = 1
                            self.n_tlinks += 1
                            
                    parent = self.nodes[parent]

    def nLinks(self):
        return self.n_links

    def nTransitiveLinks(self):
        return self.n_tlinks

    def countTransitiveLinks(self):
        n = 0
        for k1 in list(self.nodes):
            for k2 in list(self.nodes):
                if k1 != k2:
                    if self.transitivelyConnectedRecurse(k1, k2):
                        n += 1
        return n

    def directlyConnected(self, node1, node2):
        return (node1 in self.links) and (node2 in self.links[node1])

    def transitivelyConnected(self, node1, node2):
        return (node1 in self.tlinks) and (node2 in self.tlinks[node2])

    def transitivelyConnectedRecurse(self, node1, node2):
        if(self.directlyConnected(node1, node2)):
            return True
        elif not node1 in self.links:
            return False
        else:
            for k in self.links[node1]:
                if self.transitivelyConnectedRecurse(k, node2):
                    return True
            return False

    def indirectlyConnectedRecurse(self, node1, node2):
        return (not self.directlyConnected(node1, node2)) \
          and self.transitivelyConnectedRecurse(node1, node2)

    def printDotGraph(self, file = sys.stdout):
        print("digraph tree {", file = file)
        for k in self.links:
            for j in self.links[k]:
                print("  \"", k, "\" -> \"", j, "\";", file = file)
        print("}", file = file)

    def meet(self, node1, node2):
        parents = {}
        parent = self.nodes[node1]
        i = 0
        while parent != None:
            i += 1
            parents[parent] = i
            parent = self.nodes[parent]

        parent = self.nodes[node2]
        i = 0
        while parent != None:
            i += 1
            if parent in parents:
                print(node1, "meets", node2, "at", parent, "--", node1,
                          "in", parents[parent], "jumps and", node2, "in",
                          i, "jumps, totalling", parents[parent] + i)
                return parent
            parent = self.nodes[parent]

        return None

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"
    
    input_arr = []
    with open(input_file) as file:
        for line in file:
            input_arr.append(line[0:(len(line) - 1)])

    test_arr = ["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I",
                    "E)J", "J)K", "K)L"]

    test_tree = Tree(test_arr, rev = True)
    nt_links = test_tree.nTransitiveLinks()
    if(nt_links == 42):
        print("Test 1 -- passed")
    else:
        print("Test 1 -- FAILED:", nt_links)

    test2_arr = ["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I",
                     "E)J", "J)K", "K)L", "K)YOU", "I)SAN"]
    test2_tree = Tree(test2_arr, rev = True)
    test2_tree.meet("YOU", "SAN")

    input_tree = Tree(input_arr, rev = True)
    print("Part 1 answer:", input_tree.nTransitiveLinks())

    with open("input.dot", "w") as file:
        input_tree.printDotGraph(file = file)
        
    # print("Part 1 slow answer:", input_tree.countTransitiveLinks())
    print("Part 2:")
    if(input_tree.meet("YOU", "SAN") == None):
        print("YOU and SAN do not meet")
        
    sys.exit(0)

if __name__ == "__main__":
    main(sys.argv)
