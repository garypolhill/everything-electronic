#!/usr/bin/env python3

import sys, png

class Image:

    def __init__(self, wide, tall, text):
        self.layers = []

        rows = []
        columns = []

        self.n_layers = 0
        self.n_rows = tall
        self.n_columns = wide
        rc = 0
        cc = 0
        np = 0
        for pixel in text:
            if pixel != "\n":
                columns.append(int(pixel))
                cc += 1

                if cc == wide:
                    rows.append(columns)
                    columns = []
                    rc += 1

                    if rc == tall:
                        self.layers.append(rows)
                        rows = []
                        self.n_layers += 1
                        rc = 0
                        
                    cc = 0
                np += 1
                


        if len(rows) != 0:
            print(len(rows), "rows and", len(columns), "columns not added")
        elif len(columns) != 0:
            print(len(columns), "columns not added", columns)

        if np != len(text):
            print(np, "characters processed but", len(text),
                      "characters given", file = sys.stderr)

        if self.n_layers != len(self.layers):
            print("Counted number of layers", self.n_layers,
                      "not equal to length of layers array", len(self.layers),
                      file = sys.stderr)

    def nRows(self):
        return self.n_rows

    def nColumns(self):
        return self.n_columns

    def nLayers(self):
        return self.n_layers

    def nRows(self, layer):
        return len(self.layers[layer])

    def nColumns(self, layer, row):
        return len(self.layers[layer][row])

    def fewest0Layer(self):
        bestLayer = -1
        bestLayer0 = None

        for layer in range(len(self.layers)):
            layer0 = 0

            for row in range(len(self.layers[layer])):
                for column in range(len(self.layers[layer][row])):
                    if self.layers[layer][row][column] == 0:
                        layer0 += 1

            if bestLayer == -1 or layer0 < bestLayer0:
                bestLayer = layer
                bestLayer0 = layer0

        return bestLayer

    def onesByTwos(self, layer):
        layer1 = 0
        layer2 = 0

        for row in range(len(self.layers[layer])):
            for column in range(len(self.layers[layer][row])):
                if self.layers[layer][row][column] == 1:
                    layer1 += 1
                elif self.layers[layer][row][column] == 2:
                    layer2 += 1

        return layer1 * layer2

    def checkLayer(self, layer):

        if len(self.layers[layer]) != self.n_rows:
            return False

        for row in range(len(self.layers[layer])):
            if len(self.layers[layer][row]) != self.n_columns:
                return False
                
        return True

    def checkLayers(self):
        for layer in range(len(self.layers)):
            if not self.checkLayer(layer):
                return layer
        return -1

    def printLayers(self):
        for layer in range(len(self.layers)):
            print("Layer", layer + 1)
            for row in range(len(self.layers[layer])):
                print("\t", self.layers[layer][row])

    def decode(self):
        img = []
        for i in range(self.n_rows):
            row = []
            for j in range(self.n_columns):
                for k in range(self.n_layers):
                    c = self.layers[k][i][j]
                    if c == 0 or c == 1:
                        row.append(c * 255)
                        break
            img.append(row)
        return img
                

    @staticmethod
    def getImages(file_name, width = 25, height = 6):
        images = []
        with open(file_name) as f:
            for line in f:
                images.append(Image(width, height, line))
        return images

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    test = Image(3, 2, "123456789012\n")
    test.printLayers()
    
    images = Image.getImages(input_file)

    layerCheck = images[0].checkLayers()
    if(layerCheck == -1):
        print("Image has", images[0].nLayers(), "layers")
        fewest0 = images[0].fewest0Layer()
        print("Part 1 answer: Layer", fewest0,
                  "number of ones times number of twos",
                  images[0].onesByTwos(fewest0))
    else:
        print("There's a problem with layer", layerCheck)
        print("\tNumber of layers:", images[0].nLayers())
        for layer in range(images[0].nLayers()):
            if not images[0].checkLayer(layer):
                print("\t\tLayer", layer, "has", images[0].nRows(layer), "rows")
                for row in range(images[0].nRows(layer)):
                    print("\t\t\tRow", row, "has",
                              images[0].nColumns(layer, row), "columns")

    img = images[0].decode()
    with open("input.png", "wb") as f:
        w = png.Writer(images[0].nColumns(0, 0), images[0].nRows(0))
        w.write(f, img)
    print("Image written")
                    
    sys.exit(0)

if __name__ == "__main__":
    main(sys.argv)
