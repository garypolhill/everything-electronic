#!/usr/bin/env python3

# 2a: Measure times
# 2b: Generate pattern array during __init__() instead of nextPhase()
# 2c: Remove immutability: nextPhase() returns self
# 2d: Use numpy arrays to store the data
# 2e: Return to generating pattern array on the fly, don't bother returning
#     anything from nextPhase()
# 2f: Assume the base pattern will always be [0, 1, 0, -1]. The following
#     time savings can be made that way:
#        (a) The first i digits in the pattern will be zero; at a certain point
#            we can just do a sum instead of a dot product
#        (b) The same point, together with the fact that the puzzle answer needs
#            the eight digits starting at the index of the first 7 characters
#            (in my case, 5972877), means we can ignore all of the digits in
#            the string before that

import sys, time, re, numpy as np

class FFT:
    def __init__(self, string, phase = 0, repeat = 1, start = 0):
        self.start = start
        self.size = len(string) * repeat
        self.nums = np.zeros(self.size, dtype = int)
        self.phase = phase
        self.timer = 0
        self.pat = np.array([0, 1, 0, -1])
        self.n = 4

        for i in range(start, self.size):
            c = i % len(string)
            self.nums[i] = int(string[c])

    def getPhase(self):
        return self.phase

    def getSize(self):
        return self.size

    def getTimer(self):
        return self.timer

    def nextPhase(self):
        newNums = np.zeros(self.size, dtype = int)
        sum_start = (self.size + 2) // 3
        for i in range(self.start, self.size):
            if i < sum_start:
                pattern = self.ixpat(i)
                tot = np.inner(self.nums, pattern)
            else:
                last = min(self.size, (2 * i) + 1)
                tot = np.sum(self.nums[i:last:1])

            totstr = str(int(tot))
            newNums[i] = int(totstr[-1])

        self.nums = newNums
        self.phase += 1

    def outputList(self, digits = 0):
        if digits == 0:
            digits = self.size - self.start
        string = ""
        for i in range(digits):
            string += str(self.nums[i + self.start])
        return string

    def outputLast(self, last = 0):
        if last == 0:
            last = self.size
        string = ""
        for i in range(self.size - last, self.size):
            string += str(self.nums[i])
        return string

    def toPhase(self, phase):
        if self.phase > phase:
            return None
        elif self.phase == phase:
            return self
        
        t0 = time.time_ns()
        while self.getPhase() < phase:
            self.nextPhase()
        self.timer = time.time_ns() - t0

    def ixpat(self, element):
        return np.fromfunction(lambda i: self.pat[((i + 1) // (element + 1)) % self.n], (self.size,), dtype = int)
    
def test(ID, input_signal, n_phases, output):
    fft = FFT(input_signal)
    fft.toPhase(n_phases)
    if fft.outputList(len(output)) == output:
        print("Test", ID, input_signal, "to phase", n_phases, "-- passed")
    else:
        print("Test", ID, input_signal, "to phase", n_phases, "-- FAILED")
        print("\tExpected", output, "-- got", fft.outputList(len(output)))

def testIgnoreOK(ID, input_signal, n_phases, last = 8):
    fft0 = FFT(input_signal)
    fft0.toPhase(n_phases)
    fft1 = FFT(input_signal, start = len(input_signal) - last)
    fft1.toPhase(n_phases)
    if(fft0.outputLast(last) == fft1.outputLast(last)):
        print("Ignore", ID, input_signal, "to phase", n_phases, "-- passed")
    else:
        print("Ignore", ID, input_signal, "to phase", n_phases, "-- FAILED")
        print("\tExpected", fft0.outputLast(last), "-- got",
                  fft1.outputLast(last))

def timeStudy(input_signal, samples = 1, file_name = "metrics.csv",
                  rpts = [1, 2, 3, 4, 5, 6, 7, 8],
                  phases = [1, 2, 3, 4, 5, 6, 7, 8],
                  starts = [0, 80, 160, 240, 320, 400, 480, 560]):
    with open(file_name, "w") as f:
        f.write("signal.length,sample,phase,start,ns\n")
        print("signal.length,sample,phase,start,ns")
        for sample in range(samples):
            for st in starts:
                for rpt in rpts:
                    for phase in phases:
                        fft = FFT(input_signal, repeat = rpt, start = st)
                        fft.toPhase(phase)
                        f.write("%d,%d,%d,%d,%d\n"%(fft.getSize(), sample + 1,
                                                    phase, st, fft.getTimer()))
                        print("%d,%d,%d,%d,%d"%(fft.getSize(), sample + 1,
                                                phase, st, fft.getTimer()))

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    test(1, "12345678", 1, "48226158")
    test(1, "12345678", 2, "34040438")
    test(1, "12345678", 3, "03415518")
    test(1, "12345678", 4, "01029498")
    for i in range(1, 8):
        testIgnoreOK(1, "12345678", 4, i)
    test(2, "80871224585914546619083218645595", 100, "24176176")
    testIgnoreOK(2, "80871224585914546619083218645595", 100)
    test(3, "19617804207202209144916044189917", 100, "73745418")
    testIgnoreOK(3, "19617804207202209144916044189917", 100)
    test(4, "69317163492948606335995924319873", 100, "52432133")
    testIgnoreOK(4, "69317163492948606335995924319873", 100)

    all_digits = re.compile("\d+")
    with open(input_file) as f:
        for line in f:
            line = line[:-1]
            if len(line) > 0 and all_digits.fullmatch(line) != None:
                fft = FFT(line)
                print("FFT size is", fft.getSize())
                start = int(line[0:7])
                print("Big FFT start would be", start)
                fft.toPhase(100)
                print("First 8 characters after", fft.getPhase(), "phases:",
                          fft.outputList(8))
                timeStudy(line, file_name = "metrics-2f.csv")
                # Output from lm() in R (sl2 is signal.length^2, s is ns / 1e9):
                # 
                # Call:
                # lm(formula = s ~ phase + sl2, data = data)
                #
                # Coefficients:
                # (Intercept)        phase          sl2  
                #  -2.428e+01    5.385e+00    2.258e-06        # 2a
                #  -3.556e+01    7.832e+00    3.314e-06        # 2b
                #  -2.775e+01    6.070e+00    2.573e-06        # 2c
                #  -7.019e-02    1.960e-02    6.928e-09        # 2d
                #  -1.824e+00    4.350e-01    1.694e-07        # 2e
                #  -6.295e-01    1.538e-01    5.859e-08        # 2f, start = 0
                #  -5.670e-01    1.224e-01    5.281e-08        # 2f, start = 320
                #  -4.736e-01    9.272e-02    4.407e-08        # 2f, start = 560
                #
                # In 2a, each phase adds 5.4 seconds, and each square of signal
                # length is 2.3 microseconds. Though that seems small, for
                # an input of length 650 * 10000 (which is 6.5 million), sl2
                # is 4.2e13 (42 trillion!), which is 95 million seconds, or
                # about 3 years.
                #
                # 2b just makes things worse. That's because although we'd
                # moved the building of the patterns to __init__(), each time
                # we calculated for a new phase, we constructed a new FFT,
                # so nothing was saved. It's a bit weird that it takes longer,
                # but this could be the extra lookup time taken for accessing
                # the ixpat matrix.
                #
                # 2c didn't yield the hoped for improvement, though it is
                # better than 2b; the larger factor for 2b's phase than 2a's,
                # could have told us it must be something to do with the
                # lookups before we discovered 2c's phase factor is also
                # bigger than 2a's. Python's lists are clearly too slow
                # to do what we need.
                #
                # 2d made a huge improvement: nearly three orders of magnitude,
                # but is it quick enough? It's not ideal. We're down to about 3
                # days (292,708 seconds). That is at least feasible -- we
                # will get an answer, but there's another problem: our ixpat
                # matrix for a string of length 6.5 million is going to occupy
                # a huge amount of RAM. With four bytes per integer, that's 154
                # Tebibytes, or 169 Terabytes.
                #
                # 2e, which brings back the calculation of the pattern on the
                # fly, has a significant cost, and our estimate has increased
                # to 83 days. But this change was necessary to make sure we
                # didn't run out of RAM.
                #
                # 2f introduced two time-saving activities, and these both
                # have an effect. With start = 0, time is already saved over
                # 2e, bringing the time down to 29 days. With start = 560, the
                # time is 22 days. The relationship is nonlinear, but start will
                # be over five million, so we can hope that we are now in a
                # position to calculate the required answer in reasonable time

if __name__ == "__main__":
    main(sys.argv)
