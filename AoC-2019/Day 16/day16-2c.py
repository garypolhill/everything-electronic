#!/usr/bin/env python3

# 2a: Measure times
# 2b: Generate pattern array during __init__() instead of nextPhase()
# 2c: Remove immutability: nextPhase() returns self

import sys, time, re

class FFT:
    def __init__(self, string, phase = 0, repeat = 1, pattern = [0, 1, 0, -1]):
        self.nums = []
        self.phase = phase
        self.timer = 0
        self.ixpat = []
        for r in range(repeat):
            for c in string:
                self.nums.append(int(c))

        for i in range(len(self.nums)):
            ix = 0
            ixpat_i = []
            for j in range(len(pattern)):
                for k in range(i + 1):
                    ixpat_i.append(pattern[ix])
                ix += 1
                if ix == len(pattern):
                    ix = 0
                if len(ixpat_i) > len(self.nums) + 1:
                    break
            self.ixpat.append(ixpat_i)


    def getPhase(self):
        return self.phase

    def size(self):
        return len(self.nums)

    def getTimer(self):
        return self.timer

    def nextPhase(self):
        newNums = []
        for i in range(len(self.nums)):
            tot = 0
            ix = 1
            for k in range(len(self.nums)):
                tot += self.ixpat[i][ix] * self.nums[k]
                ix += 1
                if ix == len(self.ixpat[i]):
                    ix = 0

            totstr = str(tot)
            newNums.append(int(totstr[-1]))

        self.nums = newNums
        self.phase += 1
        return self

    def outputList(self, digits = 0):
        if digits == 0:
            digits = len(self.nums)
        string = ""
        for i in range(digits):
            string += str(self.nums[i])
        return string

    def toPhase(self, phase):
        if self.phase > phase:
            return None
        elif self.phase == phase:
            return self
        
        fft = FFT(self.outputList())
        t0 = time.time_ns()
        while fft.getPhase() < phase:
            fft = fft.nextPhase()
        self.timer = time.time_ns() - t0

        return fft

def test(ID, input_signal, n_phases, output):
    fft = FFT(input_signal)
    fft = fft.toPhase(n_phases)
    if fft.outputList(len(output)) == output:
        print("Test", ID, input_signal, "to phase", n_phases, "-- passed")
    else:
        print("Test", ID, input_signal, "to phase", n_phases, "-- FAILED")
        print("\tExpected", output, "-- got", fft.outputList(len(output)))

def timeStudy(input_signal, samples = 1, file_name = "metrics.csv",
                  rpts = [1, 2, 3, 4, 5, 6, 7, 8],
                  phases = [1, 2, 3, 4, 5, 6, 7, 8]):
    with open(file_name, "w") as f:
        f.write("signal.length,sample,phase,ns\n")
        print("signal.length,sample,phase,ns")
        for sample in range(samples):
            for rpt in rpts:
                for phase in phases:
                    fft = FFT(input_signal, repeat = rpt)
                    fft.toPhase(phase)
                    f.write("%d,%d,%d,%d\n"%(fft.size(), sample + 1,
                                                 phase, fft.getTimer()))
                    print("%d,%d,%d,%d"%(fft.size(), sample + 1,
                                             phase, fft.getTimer()))

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    test(1, "12345678", 1, "48226158")
    test(1, "12345678", 2, "34040438")
    test(1, "12345678", 3, "03415518")
    test(1, "12345678", 4, "01029498")
    test(2, "80871224585914546619083218645595", 100, "24176176")
    test(3, "19617804207202209144916044189917", 100, "73745418")
    test(4, "69317163492948606335995924319873", 100, "52432133")

    all_digits = re.compile("\d+")
    with open(input_file) as f:
        for line in f:
            line = line[:-1]
            if len(line) > 0 and all_digits.fullmatch(line) != None:
                fft = FFT(line)
                print("FFT size is", fft.size())
                fft = fft.toPhase(100)
                print("First 8 characters after", fft.getPhase(), "phases:",
                          fft.outputList(8))
                timeStudy(line, file_name = "metrics-2c.csv")
                # Output from lm() in R (sl2 is signal.length^2, s is ns / 1e9):
                # 
                # Call:
                # lm(formula = s ~ phase + sl2, data = data)
                #
                # Coefficients:
                # (Intercept)        phase          sl2  
                #  -2.428e+01    5.385e+00    2.258e-06        # 2a
                #  -3.556e+01    7.832e+00    3.314e-06        # 2b
                #
                # In 2a, each phase adds 5.4 seconds, and each square of signal
                # length is 2.3 microseconds. Though that seems small, for
                # an input of length 650 * 10000 (which is 6.5 million), sl2
                # is 4.2e13 (42 trillion!), which is 95 million seconds, or
                # about 3 years.
                #
                # 2b just makes things worse. That's because although we'd
                # moved the building of the patterns to __init__(), each time
                # we calculated for a new phase, we constructed a new FFT,
                # so nothing was saved. It's a bit weird that it takes longer,
                # but this could be the extra lookup time taken for accessing
                # the ixpat matrix.

if __name__ == "__main__":
    main(sys.argv)
