#!/usr/bin/env python3

import sys
import re

class FFT:
    def __init__(self, string, phase = 0):
        self.nums = []
        self.phase = phase
        for c in string:
            self.nums.append(int(c))

    def getPhase(self):
        return self.phase

    def size(self):
        return len(self.nums)

    def nextPhase(self, pattern = [0, 1, 0, -1]):
        string = ""
        for i in range(len(self.nums)):
            ix = 0
            ixpat = []
            for j in range(len(pattern)):
                for k in range(i + 1):
                    ixpat.append(pattern[ix])
                ix += 1
                if ix == len(pattern):
                    ix = 0
                if len(ixpat) > len(self.nums) + 1:
                    break

            tot = 0
            ix = 1
            for k in range(len(self.nums)):
                tot += ixpat[ix] * self.nums[k]
                ix += 1
                if ix == len(ixpat):
                    ix = 0

            totstr = str(tot)
            string += totstr[-1]
                
        return FFT(string, self.phase + 1)

    def outputList(self, digits = 0):
        if digits == 0:
            digits = len(self.nums)
        string = ""
        for i in range(digits):
            string += str(self.nums[i])
        return string

    def toPhase(self, phase):
        if self.phase > phase:
            return None
        elif self.phase == phase:
            return self
        
        fft = FFT(self.outputList())
        while fft.getPhase() < phase:
            fft = fft.nextPhase()

        return fft

def test(ID, input_signal, n_phases, output):
    fft = FFT(input_signal)
    fft = fft.toPhase(n_phases)
    if fft.outputList(len(output)) == output:
        print("Test", ID, input_signal, "to phase", n_phases, "-- passed")
    else:
        print("Test", ID, input_signal, "to phase", n_phases, "-- FAILED")
        print("\tExpected", output, "-- got", fft.outputList(len(output)))

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    test(1, "12345678", 1, "48226158")
    test(1, "12345678", 2, "34040438")
    test(1, "12345678", 3, "03415518")
    test(1, "12345678", 4, "01029498")
    test(2, "80871224585914546619083218645595", 100, "24176176")
    test(3, "19617804207202209144916044189917", 100, "73745418")
    test(4, "69317163492948606335995924319873", 100, "52432133")

    all_digits = re.compile("\d+")
    with open(input_file) as f:
        for line in f:
            line = line[:-1]
            if len(line) > 0 and all_digits.fullmatch(line) != None:
                fft = FFT(line)
                print("FFT size is", fft.size())
                fft = fft.toPhase(100)
                print("First 8 characters after", fft.getPhase(), "phases:",
                          fft.outputList(8))

if __name__ == "__main__":
    main(sys.argv)
