#!/usr/bin/env python3

# 2a: Measure times
# 2b: Generate pattern array during __init__() instead of nextPhase()
# 2c: Remove immutability: nextPhase() returns self
# 2d: Use numpy arrays to store the data

import sys, time, re, numpy as np

class FFT:
    def __init__(self, string, phase = 0, repeat = 1, pattern = [0, 1, 0, -1]):
        self.size = len(string) * repeat
        self.nums = np.zeros(self.size, dtype = int)
        self.phase = phase
        self.timer = 0
        self.ixpat = np.zeros((self.size, self.size), dtype = int)
        i = 0
        for r in range(repeat):
            for c in string:
                self.nums[i] = int(c)
                i += 1

        for i in range(self.size):
            ix = 0
            k = 0
            for j in range(-1, self.size):
                if j >= 0:
                    self.ixpat[i, j] = pattern[ix]
                k += 1
                if k > i:
                    k = 0
                    ix += 1
                    if ix == len(pattern):
                        ix = 0

#        if self.size <= 8:
#            print(self.ixpat)

    def getPhase(self):
        return self.phase

    def getSize(self):
        return self.size

    def getTimer(self):
        return self.timer

    def nextPhase(self):
        newNums = np.zeros(self.size, dtype = int)
        for i in range(self.size):
            tot = np.dot(self.nums, self.ixpat[i,:])

            totstr = str(int(tot))
            newNums[i] = int(totstr[-1])

        self.nums = newNums
        self.phase += 1
        return self

    def outputList(self, digits = 0):
        if digits == 0:
            digits = len(self.nums)
        string = ""
        for i in range(digits):
            string += str(self.nums[i])
        return string

    def toPhase(self, phase):
        if self.phase > phase:
            return None
        elif self.phase == phase:
            return self
        
        fft = FFT(self.outputList())
        t0 = time.time_ns()
        while fft.getPhase() < phase:
            fft = fft.nextPhase()
        self.timer = time.time_ns() - t0

        return fft
    
def test(ID, input_signal, n_phases, output):
    fft = FFT(input_signal)
    fft = fft.toPhase(n_phases)
    if fft.outputList(len(output)) == output:
        print("Test", ID, input_signal, "to phase", n_phases, "-- passed")
    else:
        print("Test", ID, input_signal, "to phase", n_phases, "-- FAILED")
        print("\tExpected", output, "-- got", fft.outputList(len(output)))

def timeStudy(input_signal, samples = 1, file_name = "metrics.csv",
                  rpts = [1, 2, 3, 4, 5, 6, 7, 8],
                  phases = [1, 2, 3, 4, 5, 6, 7, 8]):
    with open(file_name, "w") as f:
        f.write("signal.length,sample,phase,ns\n")
        print("signal.length,sample,phase,ns")
        for sample in range(samples):
            for rpt in rpts:
                for phase in phases:
                    fft = FFT(input_signal, repeat = rpt)
                    fft.toPhase(phase)
                    f.write("%d,%d,%d,%d\n"%(fft.getSize(), sample + 1,
                                                 phase, fft.getTimer()))
                    print("%d,%d,%d,%d"%(fft.getSize(), sample + 1,
                                             phase, fft.getTimer()))

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    test(1, "12345678", 1, "48226158")
    test(1, "12345678", 2, "34040438")
    test(1, "12345678", 3, "03415518")
    test(1, "12345678", 4, "01029498")
    test(2, "80871224585914546619083218645595", 100, "24176176")
    test(3, "19617804207202209144916044189917", 100, "73745418")
    test(4, "69317163492948606335995924319873", 100, "52432133")

    all_digits = re.compile("\d+")
    with open(input_file) as f:
        for line in f:
            line = line[:-1]
            if len(line) > 0 and all_digits.fullmatch(line) != None:
                fft = FFT(line)
                print("FFT size is", fft.getSize())
                fft = fft.toPhase(100)
                print("First 8 characters after", fft.getPhase(), "phases:",
                          fft.outputList(8))
                timeStudy(line, file_name = "metrics-2d.csv")
                # Output from lm() in R (sl2 is signal.length^2, s is ns / 1e9):
                # 
                # Call:
                # lm(formula = s ~ phase + sl2, data = data)
                #
                # Coefficients:
                # (Intercept)        phase          sl2  
                #  -2.428e+01    5.385e+00    2.258e-06        # 2a
                #  -3.556e+01    7.832e+00    3.314e-06        # 2b
                #  -2.851e+01    6.217e+00    2.676e-06        # 2c
                #  -7.019e-02    1.960e-02    6.928e-09        # 2d
                #
                # In 2a, each phase adds 5.4 seconds, and each square of signal
                # length is 2.3 microseconds. Though that seems small, for
                # an input of length 650 * 10000 (which is 6.5 million), sl2
                # is 4.2e13 (42 trillion!), which is 95 million seconds, or
                # about 3 years.
                #
                # 2b just makes things worse. That's because although we'd
                # moved the building of the patterns to __init__(), each time
                # we calculated for a new phase, we constructed a new FFT,
                # so nothing was saved. It's a bit weird that it takes longer,
                # but this could be the extra lookup time taken for accessing
                # the ixpat matrix.
                #
                # 2c didn't yield the hoped for improvement, though it is
                # better than 2b; the larger factor for 2b's phase than 2a's,
                # could have told us it must be something to do with the
                # lookups before we discovered 2c's phase factor is also
                # bigger than 2a's. Python's lists are clearly too slow
                # to do what we need.
                #
                # 2d made a huge improvement: nearly three orders of magnitude,
                # but is it quick enough? Not really. We're down to about 3
                # days (292,708 seconds). That is at least feasible -- we
                # will get an answer, but there's another problem: our ixpat
                # matrix for a string of length 6.5 million is going to occupy
                # a huge amount of RAM. With four bytes per integer, that's 154
                # Tebibytes, or 169 Terabytes.

if __name__ == "__main__":
    main(sys.argv)
