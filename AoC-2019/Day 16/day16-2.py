#!/usr/bin/env python3

# 2a: Measure times
# 2b: Generate pattern array during __init__() instead of nextPhase()
# 2c: Remove immutability: nextPhase() returns self
# 2d: Use numpy arrays to store the data
# 2e: Return to generating pattern array on the fly, don't bother returning
#     anything from nextPhase()
# 2f: Assume the base pattern will always be [0, 1, 0, -1]. The following
#     time savings can be made that way:
#        (a) The first i digits in the pattern will be zero; at a certain point
#            we can just do a sum instead of a dot product
#        (b) The same point, together with the fact that the puzzle answer needs
#            the eight digits starting at the index of the first 7 characters
#            (in my case, 5972877), means we can ignore all of the digits in
#            the string before that

import sys, time, re, numpy as np

class FFT:
    def __init__(self, string, phase = 0, repeat = 1, start = 0):
        self.start = start
        self.size = len(string) * repeat
        self.nums = np.zeros(self.size, dtype = int)
        self.phase = phase
        self.timer = 0
        self.pat = np.array([0, 1, 0, -1])
        self.n = 4

        for i in range(start, self.size):
            c = i % len(string)
            self.nums[i] = int(string[c])

    def getPhase(self):
        return self.phase

    def getSize(self):
        return self.size

    def getTimer(self):
        return self.timer

    def nextPhase(self):
        newNums = np.zeros(self.size, dtype = int)
        sum_start = (self.size + 2) // 3
        for i in range(self.start, self.size):
            if i < sum_start:
                pattern = self.ixpat(i)
                tot = np.inner(self.nums, pattern)
            else:
                last = min(self.size, (2 * i) + 1)
                tot = np.sum(self.nums[i:last:1])

            totstr = str(int(tot))
            newNums[i] = int(totstr[-1])

        self.nums = newNums
        self.phase += 1

    def outputList(self, digits = 0):
        if digits == 0:
            digits = self.size - self.start
        string = ""
        for i in range(digits):
            string += str(self.nums[i + self.start])
        return string

    def outputLast(self, last = 0):
        if last == 0:
            last = self.size
        string = ""
        for i in range(self.size - last, self.size):
            string += str(self.nums[i])
        return string

    def toPhase(self, phase, printTimeEachPhase = False):
        if self.phase > phase:
            return None
        elif self.phase == phase:
            return self
        
        t0 = time.time_ns()
        while self.getPhase() < phase:
            tp0 = time.time_ns()
            self.nextPhase()
            if printTimeEachPhase:
                tp = (time.time_ns() - tp0) / 6e10
                print("Phase", self.getPhase(), "--", tp, "minutes")
        self.timer = time.time_ns() - t0

    def ixpat(self, element):
        return np.fromfunction(lambda i: self.pat[((i + 1) // (element + 1)) % self.n], (self.size,), dtype = int)
    
def test(ID, input_signal, n_phases, output):
    fft = FFT(input_signal)
    fft.toPhase(n_phases)
    if fft.outputList(len(output)) == output:
        print("Test", ID, input_signal, "to phase", n_phases, "-- passed")
    else:
        print("Test", ID, input_signal, "to phase", n_phases, "-- FAILED")
        print("\tExpected", output, "-- got", fft.outputList(len(output)))

def testIgnoreOK(ID, input_signal, n_phases, last = 8):
    fft0 = FFT(input_signal)
    fft0.toPhase(n_phases)
    fft1 = FFT(input_signal, start = len(input_signal) - last)
    fft1.toPhase(n_phases)
    if(fft0.outputLast(last) == fft1.outputLast(last)):
        print("Ignore", ID, input_signal, "to phase", n_phases, "-- passed")
    else:
        print("Ignore", ID, input_signal, "to phase", n_phases, "-- FAILED")
        print("\tExpected", fft0.outputLast(last), "-- got",
                  fft1.outputLast(last))

def test2(ID, input_signal, n_phases, output, repeat = 10000):
    start = int(input_signal[0:7])
    fft = FFT(input_signal, repeat = repeat, start = start)
    fft.toPhase(n_phases)
    if fft.outputList(len(output)) == output:
        print("Test2", ID, input_signal, "to phase", n_phases, "-- passed")
    else:
        print("Test2", ID, input_signal, "to phase", n_phases, "-- FAILED")
        print("\tExpected", output, "-- got", fft.outputList(len(output)))
        

def timeStudy(input_signal, samples = 1, file_name = "metrics.csv",
                  rpts = [1, 2, 3, 4, 5, 6, 7, 8],
                  phases = [1, 2, 3, 4, 5, 6, 7, 8],
                  starts = [0, 80, 160, 240, 320, 400, 480, 560]):
    with open(file_name, "w") as f:
        f.write("signal.length,sample,phase,start,ns\n")
        print("signal.length,sample,phase,start,ns")
        for sample in range(samples):
            for st in starts:
                for rpt in rpts:
                    for phase in phases:
                        fft = FFT(input_signal, repeat = rpt, start = st)
                        fft.toPhase(phase)
                        f.write("%d,%d,%d,%d,%d\n"%(fft.getSize(), sample + 1,
                                                    phase, st, fft.getTimer()))
                        print("%d,%d,%d,%d,%d"%(fft.getSize(), sample + 1,
                                                phase, st, fft.getTimer()))

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    test(1, "12345678", 1, "48226158")
    test(1, "12345678", 2, "34040438")
    test(1, "12345678", 3, "03415518")
    test(1, "12345678", 4, "01029498")
    for i in range(1, 8):
        testIgnoreOK(1, "12345678", 4, i)
    test(2, "80871224585914546619083218645595", 100, "24176176")
    testIgnoreOK(2, "80871224585914546619083218645595", 100)
    test(3, "19617804207202209144916044189917", 100, "73745418")
    testIgnoreOK(3, "19617804207202209144916044189917", 100)
    test(4, "69317163492948606335995924319873", 100, "52432133")
    testIgnoreOK(4, "69317163492948606335995924319873", 100)

    test2(5, "03036732577212944063491565474664", 100, "84462026")
    test2(6, "02935109699940807407585447034323", 100, "78725270")
    test2(7, "03081770884921959731165446850517", 100, "53553731")

    all_digits = re.compile("\d+")
    with open(input_file) as f:
        for line in f:
            line = line[:-1]
            if len(line) > 0 and all_digits.fullmatch(line) != None:
                fft = FFT(line)
                print("FFT size is", fft.getSize())
                start = int(line[0:7])
                print("Big FFT start would be", start)
                fft.toPhase(100)
                print("First 8 characters after", fft.getPhase(), "phases:",
                          fft.outputList(8))
                bigfft = FFT(line, repeat = 10000, start = start)
                bigfft.toPhase(100, True)
                print("8 characters starting", start, "after",
                          bigfft.getPhase(), "phases:", bigfft.outputList(8))

if __name__ == "__main__":
    main(sys.argv)
