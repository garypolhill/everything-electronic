#!/usr/bin/env python3
#
# Advent of code day 1 part 1
#
# See https://adventofcode.com/2019/day/1
import sys

class Module:
    def __init__(self, mass):
        self.mass = mass

        # to find the fuel required for a module, take its mass,
        # divide by three, round down, and subtract 2
        self.fuel = Module.getFuelForMass(mass)

    def getFuel(self):
        return self.fuel

    def calcFuel(self):
        dmass = self.fuel
        while dmass > 0:
            dmass = Module.getFuelForMass(dmass)
            self.fuel += dmass
        return self.fuel

    @staticmethod
    def getFuelForMass(mass):
        fuel = (mass // 3) - 2
        return 0 if fuel < 0 else fuel

    @staticmethod
    def readInput(file_name):
        module_array = []
        with open(file_name) as f:
            for line in f:
                module_array.append(Module(int(line)))
        return module_array

def test(id, mass, fuel):
    result = (Module(mass).getFuel() == fuel)
    if result:
        print("Test", id, "-- pass")
    else:
        print("Test", id, "-- FAIL: Module(", mass, ").getFuel() is",
                  Module(mass).getFuel(), "not", fuel)
    return result

def test2(id, mass, fuel):
    m = Module(mass)
    f = m.calcFuel()
    result = (f == fuel)
    if result:
        print("Test2", id, "-- pass")
    else:
        print("Test2", id, "-- FAIL: calcFuel() for mass", mass, "is", f,
                  "not", fuel)
    return result
    
def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    # Tests
    ok = True
    ok = ok and test(1, 12, 2)
    ok = ok and test(2, 14, 2)
    ok = ok and test(3, 1969, 654)
    ok = ok and test(4, 100756, 33583)

    # Further tests
    ok = ok and test2(1, 14, 2)
    ok = ok and test2(2, 1969, 966)
    ok = ok and test2(3, 100756, 50346)
    
    # Main answer
    if ok:
        fuel_total = 0
        for module in Module.readInput(input_file):
            fuel_total += module.calcFuel()
        print("Answer:", fuel_total)

if __name__ == "__main__":
    main(sys.argv)
