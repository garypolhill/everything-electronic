#!/usr/bin/env python3
#
# Advent of code day 1 part 1
#
# See https://adventofcode.com/2019/day/1
import sys

class Module:
    def __init__(self, mass):
        self.mass = mass

        # to find the fuel required for a module, take its mass,
        # divide by three, round down, and subtract 2
        self.fuel = (mass // 3) - 2

    def getFuel(self):
        return self.fuel

    @staticmethod
    def readInput(file_name):
        module_array = []
        with open(file_name) as f:
            for line in f:
                module_array.append(Module(int(line)))
        return module_array

def test(id, mass, fuel):
    result = (Module(mass).getFuel() == fuel)
    if result:
        print("Test", id, "-- pass")
    else:
        print("Test", id, "-- FAIL: Module(", mass, ").getFuel() is",
                  Module(mass).getFuel(), "not", fuel)
    return result
    
def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    # Tests
    ok = True
    ok = ok and test(1, 12, 2)
    ok = ok and test(2, 14, 2)
    ok = ok and test(3, 1969, 654)
    ok = ok and test(4, 100756, 33583)
    
    # Main answer
    if ok:
        fuel_total = 0
        for module in Module.readInput(input_file):
            fuel_total += module.getFuel()
        print("Answer:", fuel_total)

if __name__ == "__main__":
    main(sys.argv)
