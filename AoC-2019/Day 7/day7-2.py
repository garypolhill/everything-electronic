#!/usr/bin/env python3

import csv, sys

class IntCode:

    # Arguments for instructions. 0 = read, 1 = write
    instargs = [[0, 0, 1],
                [0, 0, 1],
                [1],
                [0],
                [0, 0],
                [0, 0],
                [0, 0, 1],
                [0, 0, 1]]

    def __init__(self, program, prog_input = []):
        self.program = []
        self.initial = []
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        for instruction in program:
            try:
                self.program.append(int(instruction))
                self.initial.append(int(instruction))
            except ValueError:
                print("Ignoring non-integer instruction", instruction)
        self.PC = 0
        self.input_read = False
        self.awaiting_input = False
        self.output_written = False
        self.error_condition = False
        self.status_message = "OK"

    def copy(self):
        return IntCode(self.program, self.input)

    def reset(self, prog_input = []):
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        self.PC = 0
        self.input_read = False
        self.awaiting_input = False
        self.output_written = False
        self.error_condition = False
        self.status_message = "OK"
        for i in range(len(self.initial)):
            self.program[i] = self.initial[i]

    def inputRead(self, clear = True):
        ir = self.input_read
        if clear: self.input_read = False
        return ir

    def outputWritten(self, clear = True):
        ow = self.output_written
        if clear: self.output_written = False
        return ow

    def awaitingInput(self, clear = False):
        ai = self.awaiting_input
        if clear: self.awaiting_input = False
        return ai

    def errorCondition(self):
        return self.error_condition

    def statusMessage(self):
        return self.status_message

    def addInput(self, prog_input):
        self.input.append(prog_input)

    def getOutput(self):
        return [self.output[i] for i in range(len(self.output))]

    def getLastOutput(self, clear = True):
        lo = self.output[-1]
        if clear: self.output = []
        return lo

    def state(self):
        return [self.program[i] for i in range(len(self.program))]

    def get(self, loc):
        if loc == None or loc < 0 or loc >= len(self.program):
            self.error_condition = True
            if loc != None:
                self.status_message = \
                  "Segment violation reading from {}".format(loc)
            return None
        return self.program[loc]

    def put(self, loc, value):
        if loc == None or loc < 0 or loc >= len(self.program):
            self.error_condition = True
            if loc != None:
                self.status_message = \
                  "Segment violation writing to {}".format(loc)
            return None
        try:
            self.program[loc] = int(value)
        except ValueError:
            self.error_condition = True
            self.status_message = \
              "Illegal value to write to {0}: {1}".format(loc, str(value))
        
    def step(self):
        if self.error_condition:
            print(self.status_message)
            return
        
        opcode = self.get(self.PC)

        i = opcode % 100
        
        n = 0
        params = []
        opargs = list(str(opcode // 100))
        opargs.reverse()

        if i >= 1 and i <= len(IntCode.instargs):
            n = len(IntCode.instargs[i - 1])

        self.PC += 1

        for j in range(n):
            param = self.get(self.PC + j)
            if (j >= len(opargs) or opargs[j] == '0'):
                if IntCode.instargs[i - 1][j] == 0:
                    param = self.get(param)
            elif j < len(opargs) and opargs[j] != '1':
                self.error_condition = True
                self.status_message = \
                  "Illegal opargs {0} from opcode {1}".format(opargs, opcode)
                return False
            params.append(param)

        self.PC += n
        
        # Addition: 
        if i == 1:
            self.put(params[2], params[0] + params[1])

        # Multiplication: 
        elif i == 2:
            self.put(params[2], params[0] * params[1])

        # Input
        elif i == 3:
            if(len(self.input) == 0):
                self.awaiting_input = True
                self.PC -= n + 1
                return False
            else:
                self.awaiting_input = False
                self.put(params[0], self.input.pop(0))
                self.input_read = True

        # Output
        elif i == 4:
            self.output.append(params[0])
            self.output_written = True

        # Jump-If-True
        elif i == 5:
            if params[0] != 0:
                self.PC = params[1]

        # Jump-If-False
        elif i == 6:
            if params[0] == 0:
                self.PC = params[1]

        # Less Than
        elif i == 7:
            if params[0] < params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Equals
        elif i == 8:
            if params[0] == params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Termination
        elif i == 99:
            self.PC -= 1                  # If step called again, terminate
            print("Termination instruction reached")
            return False

        # Unrecognised instruction
        else:
            self.error_condition = True
            self.status_message = \
              "Illegal instruction {0} from opcode {1}".format(i, opcode)
            return False
        
        return True

    def terminated(self):
        return (self.error_condition or self.get(self.PC) == 99)
            
    def run(self, maxstep = 0):
        nstep = 0
        while not self.terminated():
            self.step()
            nstep += 1
            if maxstep > 0 and nstep >= maxstep:
                print("Step limit", maxstep, "breached")
                break
        return not self.error_condition
        
    @staticmethod
    def readPrograms(file_name):
        programs = []
        with open(file_name, newline = '') as f:
            for row in csv.reader(f):
                programs.append(IntCode(row))
        return programs


def thruster(amplifiers, phases):
    prev_output = 0
    check = {}
    
    for i in range(len(amplifiers)):
        if phases[i] in check:
            return False
        else:
            check[phases[i]] = 1
            
        amplifiers[i].addInput(phases[i])
        amplifiers[i].addInput(prev_output)
        amplifiers[i].run()
        output = amplifiers[i].getOutput()
        prev_output = output[0]
        amplifiers[i].reset()

    return prev_output

def feedbackThruster(amplifiers, phases):
    prev_input = [None for i in range(len(amplifiers))]
    prev_input[0] = 0
    check = {}
    prev_status = True

    for i in range(len(amplifiers)):
        if phases[i] in check:
            return False
        else:
            check[phases[i]] = 1
            amplifiers[i].reset()
            amplifiers[i].addInput(phases[i])

    amplifiers[0].addInput(0)

    while not all([amplifiers[i].terminated() for i in range(len(amplifiers))]):
        for i in range(len(amplifiers)):
            if not amplifiers[i].terminated():
                amplifiers[i].step()

            if amplifiers[i].errorCondition():
                print("Error", amplifiers[i].statusMessage(),
                          "in amplifier", i)
                sys.exit(1)
                    
            if amplifiers[i].outputWritten():
                j = i + 1
                if j == len(amplifiers): j = 0
                amplifiers[j].addInput( \
                        amplifiers[i].getLastOutput(clear = False))

    return amplifiers[-1].getLastOutput()

def checkThruster(amplifier, phases, n_amps = 5):
    return thruster([amplifier.copy() for i in range(n_amps)], phases)

def checkFeedbackThruster(amplifier, phases, n_amps = 5):
    return feedbackThruster([amplifier.copy() for i in range(n_amps)], phases)

def maxThruster(amplifier, n_amps = 5, feedback = True, min_phase = 5,
                    max_phase = 9):
    
    amplifiers = [amplifier.copy() for i in range(n_amps)]

    phases = [min_phase for i in range(n_amps)]
    ok = True
    max_thrust = False
    max_phases = []
    while ok:
        thrust = feedbackThruster(amplifiers, phases) if feedback \
          else thruster(amplifiers, phases)
        if thrust != False and (max_thrust == False or max_thrust < thrust):
            max_thrust = thrust
            max_phases = [phases[i] for i in range(n_amps)]
        for i in range(n_amps):
            phases[i] += 1
            if phases[i] <= max_phase:
                break
            else:
                phases[i] = min_phase
                if i == n_amps - 1:
                    ok = False
    return (max_thrust, max_phases)
        

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.csv"
    programs = IntCode.readPrograms(input_file)
    print("Read", len(programs), "programs from", input_file)

    # Tests

    tests = [[3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0],
                 [3,23,3,24,1002,24,10,24,1002,23,-1,23,
                    101,5,23,23,1,24,23,23,4,23,99,0,0],
                [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
                    1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0],
                [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
                     27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5],
                     [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,
                         55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,
                         55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,
                     6,99,0,0,0,0,10]]
    thrusts = [43210, 54321, 65210, 139629729, 18216]
    phases = [[4,3,2,1,0], [0,1,2,3,4], [1,0,4,3,2], [9,8,7,6,5], [9,7,8,5,6]]
    fbs = [False, False, False, True, True]
    nps = [0, 0, 0, 5, 5]
    xps = [4, 4, 4, 9, 9]

    for i in range(len(tests)):
        prog = IntCode(tests[i])
        mxtp = maxThruster(prog, feedback = fbs[i],
                               min_phase = nps[i], max_phase = xps[i])
        if(mxtp[0] == thrusts[i] and mxtp[1] == phases[i]):
            print("Test", i + 1, "-- passed")
        else:
            ck = checkFeedbackThruster(prog, phases[i]) if fbs[i] \
              else checkThruster(prog, phases[i])
            print("Test", i + 1, "-- FAILED:", mxtp[0], "at", mxtp[1],
                      "but expected", thrusts[i], "at", phases[i],
                      "where I get", ck)

    max_thrust = maxThruster(programs[0], feedback = False, min_phase = 0,
                                 max_phase = 4)
    print("Answer to part 1:", max_thrust[0], "at", max_thrust[1])

    max_fbthst = maxThruster(programs[0])
    print("Answer to part 2:", max_fbthst[0], "at", max_fbthst[1])
    
    sys.exit(0)

if __name__ == "__main__":
    main(sys.argv)
