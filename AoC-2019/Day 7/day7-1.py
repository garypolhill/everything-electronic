#!/usr/bin/env python3

import csv, sys

class IntCode:

    # Arguments for instructions. 0 = read, 1 = write
    instargs = [[0, 0, 1],
                [0, 0, 1],
                [1],
                [0],
                [0, 0],
                [0, 0],
                [0, 0, 1],
                [0, 0, 1]]

    def __init__(self, program, prog_input = []):
        self.program = []
        self.initial = []
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        for instruction in program:
            try:
                self.program.append(int(instruction))
                self.initial.append(int(instruction))
            except ValueError:
                print("Ignoring non-integer instruction", instruction)
        self.PC = 0

    def copy(self):
        return IntCode(self.program, self.input)

    def reset(self, prog_input = []):
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        self.PC = 0
        for i in range(len(self.initial)):
            self.program[i] = self.initial[i]

    def addInput(self, prog_input):
        self.input.append(prog_input)

    def getOutput(self):
        return self.output

    def state(self):
        return self.program

    def get(self, loc):
        if loc < 0 or loc >= len(self.program):
            print("Segment violation reading from", loc)
            sys.exit(1)
        return self.program[loc]

    def put(self, loc, value):
        if loc < 0 or loc >= len(self.program):
            print("Segment violation writing to", loc)
            sys.exit(1)
        try:
            self.program[loc] = int(value)
        except ValueError:
            print("Illegal value", value)
            sys.exit(1)
        
    def step(self):
        opcode = self.get(self.PC)

        i = opcode % 100
        
        n = 0
        params = []
        opargs = list(str(opcode // 100))
        opargs.reverse()

        if i >= 1 and i <= len(IntCode.instargs):
            n = len(IntCode.instargs[i - 1])

        self.PC += 1

        for j in range(n):
            param = self.get(self.PC + j)
            if (j >= len(opargs) or opargs[j] == '0'):
                if IntCode.instargs[i - 1][j] == 0:
                    param = self.get(param)
            elif j < len(opargs) and opargs[j] != '1':
                print("Illegal opargs", opargs, "from opcode", opcode)
                sys.exit(1)
            params.append(param)

        self.PC += n
        
        # Addition: 
        if i == 1:
            self.put(params[2], params[0] + params[1])

        # Multiplication: (1??01) not relevant
        elif i == 2:
            self.put(params[2], params[0] * params[1])

        # Input
        elif i == 3:
            self.put(params[0], self.input.pop(0))

        # Output
        elif i == 4:
            self.output.append(params[0])

        # Jump-If-True
        elif i == 5:
            if params[0] != 0:
                self.PC = params[1]

        # Jump-If-False
        elif i == 6:
            if params[0] == 0:
                self.PC = params[1]

        # Less Than
        elif i == 7:
            if params[0] < params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Equals
        elif i == 8:
            if params[0] == params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Termination
        elif i == 99:
            self.PC -= 1                  # If step called again, terminate
            print("Termination instruction reached")

        # Unrecognised instruction
        else:
            print("Illegal instruction", i, "from opcode", opcode)
            sys.exit(1)

    def terminated(self):
        return (self.get(self.PC) == 99)
            
    def run(self, maxstep = 0):
        nstep = 0
        while not self.terminated():
            self.step()
            nstep += 1
            if maxstep > 0 and nstep >= maxstep:
                print("Step limit", maxstep, "breached")
                break
        
    @staticmethod
    def readPrograms(file_name):
        programs = []
        with open(file_name, newline = '') as f:
            for row in csv.reader(f):
                programs.append(IntCode(row))
        return programs


def thruster(amplifiers, phases):
    prev_output = 0
    check = {}
    
    for i in range(len(amplifiers)):
        if phases[i] in check:
            return False
        else:
            check[phases[i]] = 1
            
        amplifiers[i].addInput(phases[i])
        amplifiers[i].addInput(prev_output)
        amplifiers[i].run()
        output = amplifiers[i].getOutput()
        prev_output = output[0]
        amplifiers[i].reset()

    return prev_output

def checkThruster(amplifier, phases, n_amps = 5):
    return thruster([amplifier.copy() for i in range(n_amps)], phases)

def maxThruster(amplifier, n_amps = 5, max_phase = 5):
    amplifiers = [amplifier.copy() for i in range(n_amps)]

    phases = [0 for i in range(n_amps)]
    ok = True
    max_thrust = False
    max_phases = []
    while ok:
        thrust = thruster(amplifiers, phases)
        if thrust != False and (max_thrust == False or max_thrust < thrust):
            max_thrust = thrust
            max_phases = [phases[i] for i in range(n_amps)]
        for i in range(n_amps):
            phases[i] += 1
            if phases[i] < max_phase:
                break
            else:
                phases[i] = 0
                if i == n_amps - 1:
                    ok = False
    return (max_thrust, max_phases)
        

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.csv"
    programs = IntCode.readPrograms(input_file)
    print("Read", len(programs), "programs from", input_file)

    # Tests

    tests = [[3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0],
                 [3,23,3,24,1002,24,10,24,1002,23,-1,23,
                    101,5,23,23,1,24,23,23,4,23,99,0,0],
                [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
                    1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]]
    thrusts = [43210, 54321, 65210]
    phases = [[4,3,2,1,0], [0,1,2,3,4], [1,0,4,3,2]]

    for i in range(len(tests)):
        prog = IntCode(tests[i])
        mxtp = maxThruster(prog)
        if(mxtp[0] == thrusts[i] and mxtp[1] == phases[i]):
            print("Test", i + 1, "-- passed")
        else:
            print("Test", i + 1, "-- FAILED:", mxtp[0], "at", mxtp[1],
                      "but expected", thrusts[i], "at", phases[i],
                      "where I get", checkThruster(prog, phases[i]))

    max_thrust = maxThruster(programs[0])
    print("Answer:", max_thrust[0], "at", max_thrust[1])
    
    sys.exit(0)

if __name__ == "__main__":
    main(sys.argv)
