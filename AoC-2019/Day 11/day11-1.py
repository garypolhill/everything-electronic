#!/usr/bin/env python3

import csv, sys

class IntCode:

    # Arguments for instructions. 0 = read, 1 = write
    instargs = [[0, 0, 1],                # Add
                [0, 0, 1],                # Multiply
                [1],                      # Input
                [0],                      # Output
                [0, 0],                   # Jump if true
                [0, 0],                   # Jump if false
                [0, 0, 1],                # Less than
                [0, 0, 1],                # Equals
                [0]]                      # Adjust relative base

    def __init__(self, program, prog_input = []):
        self.program = {}
        self.initial = {}
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        i = 0
        for instruction in program:
            try:
                self.program[i] = int(instruction)
                self.initial[i] = int(instruction)
            except ValueError:
                print("Ignoring non-integer instruction", instruction)
            i += 1
        self.PC = 0
        self.RC = 0                       # Relative address position pointer
        self.input_read = False
        self.awaiting_input = False
        self.output_written = False
        self.error_condition = False
        self.status_message = "OK"

    def copy(self):
        return IntCode(self.program, self.input)

    def reset(self, prog_input = []):
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        self.PC = 0
        self.RC = 0
        self.input_read = False
        self.awaiting_input = False
        self.output_written = False
        self.error_condition = False
        self.status_message = "OK"
        self.program = {}
        for i in self.initial:
            self.program[i] = self.initial[i]

    def inputRead(self, clear = True):
        ir = self.input_read
        if clear: self.input_read = False
        return ir

    def outputWritten(self, clear = True):
        ow = self.output_written
        if clear: self.output_written = False
        return ow

    def awaitingInput(self, clear = False):
        ai = self.awaiting_input
        if clear: self.awaiting_input = False
        return ai

    def errorCondition(self):
        return self.error_condition

    def statusMessage(self):
        return self.status_message

    def addInput(self, prog_input):
        self.input.append(prog_input)

    def getOutput(self):
        return [self.output[i] for i in range(len(self.output))]

    def getLastOutput(self, clear = True):
        lo = self.output[-1]
        if clear: self.output = []
        return lo

    def clearOutput(self):
        self.output = []

    def state(self):
        return {self.program[i] for i in self.program}

    def get(self, loc):
        if loc == None or loc < 0:
            self.error_condition = True
            if loc != None:
                self.status_message = \
                  "Segment violation reading from {}".format(loc)
            return None
        if not loc in self.program:
            self.program[loc] = 0
        return self.program[loc]

    def put(self, loc, value):
        if loc == None or loc < 0:
            self.error_condition = True
            if loc != None:
                self.status_message = \
                  "Segment violation writing to {}".format(loc)
            return None
        try:
            self.program[loc] = int(value)
        except ValueError:
            self.error_condition = True
            self.status_message = \
              "Illegal value to write to {0}: {1}".format(loc, str(value))
        
    def step(self):
        if self.error_condition:
            print(self.status_message)
            return
        
        opcode = self.get(self.PC)

        i = opcode % 100
        
        n = 0
        params = []
        opargs = list(str(opcode // 100))
        opargs.reverse()

        if i >= 1 and i <= len(IntCode.instargs):
            n = len(IntCode.instargs[i - 1])

        self.PC += 1

        for j in range(n):
            param = self.get(self.PC + j)
            if j >= len(opargs) or opargs[j] == '0':
                if IntCode.instargs[i - 1][j] == 0:
                    param = self.get(param)
            elif j < len(opargs) and opargs[j] == '2':
                if IntCode.instargs[i - 1][j] == 0:
                    param = self.get(self.RC + param)
                else:
                    param = self.RC + param
            elif j < len(opargs) and opargs[j] != '1':
                self.error_condition = True
                self.status_message = \
                  "Illegal opargs {0} from opcode {1}".format(opargs, opcode)
                return False
            params.append(param)

        self.PC += n
        
        # Addition: 
        if i == 1:
            self.put(params[2], params[0] + params[1])

        # Multiplication: 
        elif i == 2:
            self.put(params[2], params[0] * params[1])

        # Input
        elif i == 3:
            if(len(self.input) == 0):
                self.awaiting_input = True
                self.PC -= n + 1
                return False
            else:
                self.awaiting_input = False
                self.put(params[0], self.input.pop(0))
                self.input_read = True

        # Output
        elif i == 4:
            self.output.append(params[0])
            self.output_written = True

        # Jump-If-True
        elif i == 5:
            if params[0] != 0:
                self.PC = params[1]

        # Jump-If-False
        elif i == 6:
            if params[0] == 0:
                self.PC = params[1]

        # Less Than
        elif i == 7:
            if params[0] < params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Equals
        elif i == 8:
            if params[0] == params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Adjust relative base
        elif i == 9:
            self.RC += params[0]
            
        # Termination
        elif i == 99:
            self.PC -= 1                  # If step called again, terminate
            print("Termination instruction reached")
            return False

        # Unrecognised instruction
        else:
            self.error_condition = True
            self.status_message = \
              "Illegal instruction {0} from opcode {1}".format(i, opcode)
            return False
        
        return True

    def terminated(self):
        return (self.error_condition or self.get(self.PC) == 99)
            
    def run(self, maxstep = 0):
        nstep = 0
        while not self.terminated():
            self.step()
            nstep += 1
            if maxstep > 0 and nstep >= maxstep:
                print("Step limit", maxstep, "breached")
                break
        return not self.error_condition

    def runUntilOutputWritten(self):
        while not (self.terminated() or self.outputWritten()):
            self.step()
        return not self.error_condition

    def runUntilAwaitingInput(self):
        while not (self.terminated() or self.awaitingInput()):
            self.step()
        return not self.error_condition
        
    @staticmethod
    def readPrograms(file_name):
        programs = []
        with open(file_name, newline = '') as f:
            for row in csv.reader(f):
                programs.append(IntCode(row))
        return programs


class Robot:

    def __init__(self, prog, init_dir = "up", default_panel = 0):
        self.prog = prog
        self.dir = init_dir
        self.panels = {}
        self.x = 0
        self.y = 0
        self.default_colour = default_panel
        self.n_painted = 0

    def getColour(self):
        if (self.x, self.y) in self.panels:
            return self.panels[self.x, self.y]
        else:
            return self.default_colour

    def move(self):
        self.prog.addInput(self.getColour())
        self.prog.runUntilOutputWritten()
        if self.prog.terminated():
            return False
        if self.prog.errorCondition():
            print(self.prog.statusMessage())
            sys.exit(1)
        clr = self.prog.getLastOutput()
        self.prog.runUntilOutputWritten()
        if self.prog.terminated():
            return False
        if self.prog.errorCondition():
            print(self.prog.statusMessage())
            sys.exit(1)
        dir_n = self.prog.getLastOutput()
        self.paint(clr)
        self.turn(dir_n)
        self.step()
        return True

    def paint(self, clr):
        if not (self.x, self.y) in self.panels:
            self.n_painted += 1
        self.panels[self.x, self.y] = clr

    def getNPainted(self):
        return self.n_painted

    def turn(self, dir_n):
        if self.dir == "up":
            self.dir = "left" if dir_n == 0 else "right"
        elif self.dir == "down":
            self.dir = "right" if dir_n == 0 else "left"
        elif self.dir == "left":
            self.dir = "down" if dir_n == 0 else "up"
        elif self.dir == "right":
            self.dir = "up" if dir_n == 0 else "down"
        else:
            print("PANIC! Direction is", self.dir, file = sys.stderr)
            sys.exit(1)

    def step(self):
        if self.dir == "up":
            self.y -= 1
        elif self.dir == "down":
            self.y += 1
        elif self.dir == "left":
            self.x -= 1
        elif self.dir == "right":
            self.x += 1
        else:
            print("PANIC! Direction is", self.dir, file = sys.stderr)
            sys.exit(1)


def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.csv"
    programs = IntCode.readPrograms(input_file)
    print("Read", len(programs), "programs from", input_file)

    robot = Robot(programs[0])

    n_moves = 0
    while robot.move():
        n_moves += 1

    print("The robot painted", robot.getNPainted(), "panels in", n_moves,
              "moves")
        
    sys.exit(0)

if __name__ == "__main__":
    main(sys.argv)
