#!/usr/bin/env python3

import csv, sys

class IntCode:

    def __init__(self, program):
        self.program = []
        for instruction in program:
            try:
                self.program.append(int(instruction))
            except ValueError:
                print("Ignoring non-integer instruction", instruction)
        self.PC = 0

    def state(self):
        return self.program

    def get(self, loc):
        if loc < 0 or loc >= len(self.program):
            print("Segment violation", loc)
            sys.exit(1)
        return self.program[loc]

    def put(self, loc, value):
        if loc < 0 or loc >= len(self.program):
            print("Segment violation", loc)
            sys.exit(1)
        try:
            self.program[loc] = int(value)
        except ValueError:
            print("Illegal value", value)
            sys.exit(1)
        
    def step(self):
        i = self.get(self.PC)
        if i == 1:                        # Addition
            self.put(self.get(self.PC + 3),
                     self.get(self.get(self.PC + 1))
                         + self.get(self.get(self.PC + 2)))
            self.PC += 4
        elif i == 2:                      # Multiplication
            self.put(self.get(self.PC + 3),
                     self.get(self.get(self.PC + 1))
                         * self.get(self.get(self.PC + 2)))
            self.PC += 4
        elif i == 99:                     # Termination
            print("Termination instruction reached")
        else:
            print("Illegal instruction", i)
            sys.exit(1)

    def terminated(self):
        return (self.get(self.PC) == 99)
            
    def run(self, maxstep = 0):
        nstep = 0
        while not self.terminated():
            self.step()
            nstep += 1
            if maxstep > 0 and nstep >= maxstep:
                print("Step limit", maxstep, "breached")
                break
        
    @staticmethod
    def readPrograms(file_name):
        programs = []
        with open(file_name, newline = '') as f:
            for row in csv.reader(f):
                programs.append(IntCode(row))
        return programs

def test(id, start, stop):
    prog = IntCode(start)
    prog.run()
    state = prog.state()
    if(state == stop):
        print("Test", id, "-- pass")
        return True
    else:
        print("Test", id, "-- FAIL")
        for i in range(len(state)):
            if(state[i] != stop[i]):
                print("\tAt", i, "state is", state[i], "but", stop[i],
                      "expected")
        return False

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.csv"
    programs = IntCode.readPrograms(input_file)
    print("Read", len(programs), "programs from", input_file)

    # Test programs
    ok = True
    ok = test(1, [1,0,0,0,99], [2,0,0,0,99]) and ok
    ok = test(2, [2,3,0,3,99], [2,3,0,6,99]) and ok
    ok = test(3, [2,4,4,5,99,0], [2,4,4,5,99,9801]) and ok
    ok = test(4, [1,1,1,4,99,5,6,0,99], [30,1,1,4,2,5,6,0,99]) and ok

    # Run the puzzle

    if(ok):
        programs[0].put(1, 12)
        programs[0].put(2, 2)
        programs[0].run()
        print("Value at position 0 after running is", programs[0].get(0))
        sys.exit(0)
    else:
        print("One or more tests failed; not running the puzzle")
        sys.exit(1)

if __name__ == "__main__":
    main(sys.argv)
