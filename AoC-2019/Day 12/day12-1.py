#!/usr/bin/env python3

import sys

class Moon:
    def __init__(self, x, y, z, vx = 0, vy = 0, vz = 0):
        self.x = x
        self.y = y
        self.z = z
        self.vx = vx
        self.vy = vy
        self.vz = vz

    def gravitate(self, other_moon):
        if self.x < other_moon.x:
            self.vx += 1
            other_moon.vx -= 1
        elif self.x > other_moon.x:
            self.vx -= 1
            other_moon.vx += 1

        if self.y < other_moon.y:
            self.vy += 1
            other_moon.vy -= 1
        elif self.y > other_moon.y:
            self.vy -= 1
            other_moon.vy += 1

        if self.z < other_moon.z:
            self.vz += 1
            other_moon.vz -= 1
        elif self.z > other_moon.z:
            self.vz -= 1
            other_moon.vz += 1

    def velocitate(self):
        self.x += self.vx
        self.y += self.vy
        self.z += self.vz

    def getPotentialEnergy(self):
        return abs(self.x) + abs(self.y) + abs(self.z)

    def getKineticEnergy(self):
        return abs(self.vx) + abs(self.vy) + abs(self.vz)

    def getTotalEnergy(self):
        return self.getPotentialEnergy() * self.getKineticEnergy()

    def printMoon(self):
        print("pos = (", self.x, ",", self.y, ",", self.z, "); vel = (",
                  self.vx, ",", self.vy, ",", self.vz, ")")

    def printMoonEnergy(self):
        print("pot =", abs(self.x), "+", abs(self.y), "+", abs(self.z), "=",
                  self.getPotentialEnergy(), "   kin =", abs(self.vx), "+",
                  abs(self.vy), "+", abs(self.vz), "=", self.getKineticEnergy(),
                  "   tot =", self.getTotalEnergy())

    @staticmethod
    def readMoons(file_name):
        moons = []
        with open(file_name) as f:
            for line in f:
                line = line[:-1]
                line = line.replace("<", "")
                line = line.replace(">", "")
                line = line.replace("=", ",")
                line = line.replace(" ", "")
                values = line.split(",")
                if values[0] == "x" and values[2] == "y" and values[4] == "z":
                    moons.append(Moon(int(values[1]),
                                      int(values[3]),
                                      int(values[5])))
                else:
                    print("Unexpected line format", values)
                    
        return moons

    @staticmethod
    def stepMoons(moons):
        for i in range(len(moons) - 1):
            for j in range(i + 1, len(moons)):
                moons[i].gravitate(moons[j])

        for i in range(len(moons)):
            moons[i].velocitate()

    @staticmethod
    def totalEnergy(moons):
        return sum([moons[i].getTotalEnergy() for i in range(len(moons))])

def test1():
    moons = [Moon(-1, 0, 2),
             Moon(2, -10, -7),
             Moon(4, -8, 8),
             Moon(3, 5, -1)]
    for moon in moons:
        moon.printMoon()

    print(" ")
    for i in range(10):
        print("After", i + 1, "steps")
        Moon.stepMoons(moons)
        for moon in moons:
            moon.printMoon()
        print(" ")
        
    print("Energy:")
    for moon in moons:
        moon.printMoonEnergy()
    print(" ")

    if Moon.totalEnergy(moons) == 179:
        print("Test 1 -- passed")
    else:
        print("Test 1 -- FAILED:", Moon.totalEnergy(moons))

def test2():
    moons = [Moon(-8, -10, 0),
             Moon(5, 5, 10),
             Moon(2, -7, 3),
             Moon(9, -8, -3)]
    for i in range(100):
        Moon.stepMoons(moons)

    for moon in moons:
        moon.printMoon()
    print(" ")
    for moon in moons:
        moon.printMoonEnergy()
    print(" ")

        
    if Moon.totalEnergy(moons) == 1940:
        print("Test 2 -- passed")
    else:
        print("Test 2 -- FAILED:", Moon.totalEnergy(moons))
    
def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    test1()
    test2()

    moons = Moon.readMoons(input_file)

    for i in range(1000):
        Moon.stepMoons(moons)

    print("Total energy after 1000 steps", Moon.totalEnergy(moons))

    sys.exit(0)

    
if __name__ == "__main__":
    main(sys.argv)

