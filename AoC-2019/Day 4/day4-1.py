#!/usr/bin/env python3

import sys

class PasswordHacker:
    def __init__(self, minpw, maxpw):
        self.minpw = int(minpw)
        self.maxpw = int(maxpw)
        self.diff = (self.maxpw - self.minpw) + 1

    @staticmethod
    def neverDecrease(candidate):
        strcan = str(candidate)
        prev = -1
        for i in strcan:
            this = int(i)
            if this < prev:
                return False
            prev = this
        return True

    @staticmethod
    def twoTheSame(candidate):
        strcan = str(candidate)
        prev = "a"
        for i in strcan:
            if i == prev:
                return True
            prev = i
        return False

    @staticmethod
    def exactlyTwoTheSame(candidate):
        strcan = str(candidate)
        prev = "a"
        status = 0
        for i in strcan:
            if i == prev or prev == "a":
                status += 1
            elif i != prev and status == 2:
                return True
            else:
                status = 1
            prev = i

        return status == 2

    def countRuleMatch(self):
        n = 0
        for i in range(self.diff):
            candidate = self.minpw + i
            if self.neverDecrease(candidate) and self.twoTheSame(candidate):
                n += 1
        return n

    def countRuleMatchExact(self):
        n = 0
        for i in range(self.diff):
            candidate = self.minpw + i
            if self.neverDecrease(candidate) and \
              self.exactlyTwoTheSame(candidate):
                n += 1
        return n

def main(argv):
    input = argv[1] if len(argv) > 1 else "183564-657474"
    minmax = input.split("-")

    # Tests
    test1 = PasswordHacker(111111, 111111)
    if(test1.countRuleMatch() == 1):
        print("Test 1 passed")
    else:
        print("Test 1 FAILED")

    test2 = PasswordHacker(223450, 223450)
    if(test2.countRuleMatch() == 0):
        print("Test 2 passed")
    else:
        print("Test 2 FAILED")

    test3 = PasswordHacker(123789, 123789)
    if(test3.countRuleMatch() == 0):
        print("Test 3 passed")
    else:
        print("Test 3 FAILED")

    test4 = PasswordHacker(112233, 112233)
    if(test4.countRuleMatchExact() == 1):
        print("Test 4 passed")
    else:
        print("Test 4 FAILED")

    test5 = PasswordHacker(123444, 123444)
    if(test5.countRuleMatchExact() == 0):
        print("Test 5 passed")
    else:
        print("Test 5 FAILED")

    test6 = PasswordHacker(111122, 111122)
    if(test6.countRuleMatchExact() == 1):
        print("Test 6 passed")
    else:
        print("Test 6 FAILED")

    part1 = PasswordHacker(minmax[0], minmax[1])
    print("Answer (1):", part1.countRuleMatch())
    print("Answer (2):", part1.countRuleMatchExact())
    
    sys.exit(0)
    
if __name__ == "__main__":
    main(sys.argv)
