#!/usr/bin/env python3

import csv, sys

class IntCode:

    # Arguments for instructions. 0 = read, 1 = write
    instargs = [[0, 0, 1],
                [0, 0, 1],
                [1],
                [0],
                [0, 0],
                [0, 0],
                [0, 0, 1],
                [0, 0, 1]]

    def __init__(self, program, prog_input = []):
        self.program = []
        self.initial = []
        self.input = prog_input
        self.output = []
        for instruction in program:
            try:
                self.program.append(int(instruction))
                self.initial.append(int(instruction))
            except ValueError:
                print("Ignoring non-integer instruction", instruction)
        self.PC = 0

    def reset(self, prog_input = []):
        self.input = prog_input
        self.output = []
        self.PC = 0
        for i in range(len(self.initial)):
            self.program[i] = self.initial[i]

    def addInput(self, prog_input):
        self.input.append(prog_input)

    def getOutput(self):
        return self.output

    def state(self):
        return self.program

    def get(self, loc):
        if loc < 0 or loc >= len(self.program):
            print("Segment violation reading from", loc)
            sys.exit(1)
        return self.program[loc]

    def put(self, loc, value):
        if loc < 0 or loc >= len(self.program):
            print("Segment violation writing to", loc)
            sys.exit(1)
        try:
            self.program[loc] = int(value)
        except ValueError:
            print("Illegal value", value)
            sys.exit(1)
        
    def step(self):
        opcode = self.get(self.PC)

        i = opcode % 100
        
        n = 0
        params = []
        opargs = list(str(opcode // 100))
        opargs.reverse()

        if i >= 1 and i <= len(IntCode.instargs):
            n = len(IntCode.instargs[i - 1])

        self.PC += 1

        for j in range(n):
            param = self.get(self.PC + j)
            if (j >= len(opargs) or opargs[j] == '0'):
                if IntCode.instargs[i - 1][j] == 0:
                    param = self.get(param)
            elif j < len(opargs) and opargs[j] != '1':
                print("Illegal opargs", opargs, "from opcode", opcode)
                sys.exit(1)
            params.append(param)

        self.PC += n
        
        # Addition: 
        if i == 1:
            self.put(params[2], params[0] + params[1])

        # Multiplication: (1??01) not relevant
        elif i == 2:
            self.put(params[2], params[0] * params[1])

        # Input
        elif i == 3:
            self.put(params[0], self.input.pop(0))

        # Output
        elif i == 4:
            self.output.append(params[0])

        # Jump-If-True
        elif i == 5:
            if params[0] != 0:
                self.PC = params[1]

        # Jump-If-False
        elif i == 6:
            if params[0] == 0:
                self.PC = params[1]

        # Less Than
        elif i == 7:
            if params[0] < params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Equals
        elif i == 8:
            if params[0] == params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Termination
        elif i == 99:
            self.PC -= 1                  # If step called again, terminate
            print("Termination instruction reached")

        # Unrecognised instruction
        else:
            print("Illegal instruction", i, "from opcode", opcode)
            sys.exit(1)

    def terminated(self):
        return (self.get(self.PC) == 99)
            
    def run(self, maxstep = 0):
        nstep = 0
        while not self.terminated():
            self.step()
            nstep += 1
            if maxstep > 0 and nstep >= maxstep:
                print("Step limit", maxstep, "breached")
                break
        
    @staticmethod
    def readPrograms(file_name):
        programs = []
        with open(file_name, newline = '') as f:
            for row in csv.reader(f):
                programs.append(IntCode(row))
        return programs

def test(id, start, stop):
    prog = IntCode(start)
    prog.run()
    state = prog.state()
    if(state == stop):
        print("Test", id, "-- pass")
        return True
    else:
        print("Test", id, "-- FAIL")
        for i in range(len(state)):
            if(state[i] != stop[i]):
                print("\tAt", i, "state is", state[i], "but", stop[i],
                      "expected")
        return False

def test5(id, program, data, expected):
    prog = IntCode(program, data)
    prog.run()
    output = prog.getOutput()
    if(output == expected):
        print("Test5", id, "-- pass")
        return True
    else:
        print("Test5", id, "-- FAIL")
        for i in range(len(output)):
            if(i < len(expected) and output[i] != expected[i]):
                print("\tAt", i, "output is", output[i], "but", expected[i],
                          "expected")
        return False

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.csv"

    programs = IntCode.readPrograms(input_file)

    if len(argv) > 2:
        maxstep = 0
        for arg in argv[2:]:
            if arg == "-maxstep":
                maxstep = -1
            elif maxstep == -1:
                maxstep = int(arg)
            else:
                programs[0].addInput(int(arg))
        programs[0].run(maxstep)
        print("Answer is", programs[0].getOutput())
        sys.exit(0)
    
    print("Read", len(programs), "programs from", input_file)

    # Test programs
    ok = True
    ok = test(1, [1,0,0,0,99], [2,0,0,0,99]) and ok
    ok = test(2, [2,3,0,3,99], [2,3,0,6,99]) and ok
    ok = test(3, [2,4,4,5,99,0], [2,4,4,5,99,9801]) and ok
    ok = test(4, [1,1,1,4,99,5,6,0,99], [30,1,1,4,2,5,6,0,99]) and ok

    # Day 5 test programs

    ok = test5(1, [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31, \
    1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104, \
    999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], [7], [999]) and ok
    
    ok = test5(2, [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31, \
    1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104, \
    999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], [8], [1000]) and ok

    ok = test5(3, [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31, \
    1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104, \
    999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], [9], [1001]) and ok

    # Run the puzzle

    if(ok):
        programs[0].addInput(1)
        programs[0].run()
        print("Output part 1:", programs[0].getOutput())

        programs[0].reset()
        programs[0].addInput(5)
        programs[0].run()
        print("Output part 2:", programs[0].getOutput())
    else:
        print("One or more tests failed; not running the puzzle")
        sys.exit(1)

if __name__ == "__main__":
    main(sys.argv)
