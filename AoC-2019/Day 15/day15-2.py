#!/usr/bin/env python3

import csv, sys, time

class IntCode:

    # Arguments for instructions. 0 = read, 1 = write
    instargs = [[0, 0, 1],                # Add
                [0, 0, 1],                # Multiply
                [1],                      # Input
                [0],                      # Output
                [0, 0],                   # Jump if true
                [0, 0],                   # Jump if false
                [0, 0, 1],                # Less than
                [0, 0, 1],                # Equals
                [0]]                      # Adjust relative base

    def __init__(self, program, prog_input = []):
        self.program = {}
        self.initial = {}
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        i = 0
        for instruction in program:
            try:
                self.program[i] = int(instruction)
                self.initial[i] = int(instruction)
            except ValueError:
                print("Ignoring non-integer instruction", instruction)
            i += 1
        self.PC = 0
        self.RC = 0                       # Relative address position pointer
        self.input_read = False
        self.awaiting_input = False
        self.output_written = False
        self.error_condition = False
        self.status_message = "OK"

    def copy(self):
        return IntCode(self.program, self.input)

    def reset(self, prog_input = []):
        self.input = [prog_input[i] for i in range(len(prog_input))]
        self.output = []
        self.PC = 0
        self.RC = 0
        self.input_read = False
        self.awaiting_input = False
        self.output_written = False
        self.error_condition = False
        self.status_message = "OK"
        self.program = {}
        for i in self.initial:
            self.program[i] = self.initial[i]

    def inputRead(self, clear = True):
        ir = self.input_read
        if clear: self.input_read = False
        return ir

    def outputWritten(self, clear = True):
        ow = self.output_written
        if clear: self.output_written = False
        return ow

    def awaitingInput(self, clear = False):
        ai = self.awaiting_input
        if clear: self.awaiting_input = False
        return ai

    def errorCondition(self):
        return self.error_condition

    def statusMessage(self):
        return self.status_message

    def addInput(self, prog_input):
        self.input.append(prog_input)

    def getOutput(self):
        return [self.output[i] for i in range(len(self.output))]

    def getLastOutput(self, clear = True):
        lo = self.output[-1]
        if clear: self.output = []
        return lo

    def clearOutput(self):
        self.output = []

    def state(self):
        return {self.program[i] for i in self.program}

    def get(self, loc):
        if loc == None or loc < 0:
            self.error_condition = True
            if loc != None:
                self.status_message = \
                  "Segment violation reading from {}".format(loc)
            return None
        if not loc in self.program:
            self.program[loc] = 0
        return self.program[loc]

    def put(self, loc, value):
        if loc == None or loc < 0:
            self.error_condition = True
            if loc != None:
                self.status_message = \
                  "Segment violation writing to {}".format(loc)
            return None
        try:
            self.program[loc] = int(value)
        except ValueError:
            self.error_condition = True
            self.status_message = \
              "Illegal value to write to {0}: {1}".format(loc, str(value))
        
    def step(self):
        if self.error_condition:
            print(self.status_message)
            return
        
        opcode = self.get(self.PC)

        i = opcode % 100
        
        n = 0
        params = []
        opargs = list(str(opcode // 100))
        opargs.reverse()

        if i >= 1 and i <= len(IntCode.instargs):
            n = len(IntCode.instargs[i - 1])

        self.PC += 1

        for j in range(n):
            param = self.get(self.PC + j)
            if j >= len(opargs) or opargs[j] == '0':
                if IntCode.instargs[i - 1][j] == 0:
                    param = self.get(param)
            elif j < len(opargs) and opargs[j] == '2':
                if IntCode.instargs[i - 1][j] == 0:
                    param = self.get(self.RC + param)
                else:
                    param = self.RC + param
            elif j < len(opargs) and opargs[j] != '1':
                self.error_condition = True
                self.status_message = \
                  "Illegal opargs {0} from opcode {1}".format(opargs, opcode)
                return False
            params.append(param)

        self.PC += n
        
        # Addition: 
        if i == 1:
            self.put(params[2], params[0] + params[1])

        # Multiplication: 
        elif i == 2:
            self.put(params[2], params[0] * params[1])

        # Input
        elif i == 3:
            if(len(self.input) == 0):
                self.awaiting_input = True
                self.PC -= n + 1
                return False
            else:
                self.awaiting_input = False
                self.put(params[0], self.input.pop(0))
                self.input_read = True

        # Output
        elif i == 4:
            self.output.append(params[0])
            self.output_written = True

        # Jump-If-True
        elif i == 5:
            if params[0] != 0:
                self.PC = params[1]

        # Jump-If-False
        elif i == 6:
            if params[0] == 0:
                self.PC = params[1]

        # Less Than
        elif i == 7:
            if params[0] < params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Equals
        elif i == 8:
            if params[0] == params[1]:
                self.put(params[2], 1)
            else:
                self.put(params[2], 0)

        # Adjust relative base
        elif i == 9:
            self.RC += params[0]
            
        # Termination
        elif i == 99:
            self.PC -= 1                  # If step called again, terminate
            print("Termination instruction reached")
            return False

        # Unrecognised instruction
        else:
            self.error_condition = True
            self.status_message = \
              "Illegal instruction {0} from opcode {1}".format(i, opcode)
            return False
        
        return True

    def terminated(self):
        return (self.error_condition or self.get(self.PC) == 99)
            
    def run(self, maxstep = 0):
        nstep = 0
        while not self.terminated():
            self.step()
            nstep += 1
            if maxstep > 0 and nstep >= maxstep:
                print("Step limit", maxstep, "breached")
                break
        return not self.error_condition

    def runUntilOutputWritten(self):
        while not (self.terminated() or self.outputWritten()):
            self.step()
        return not self.error_condition

    def runUntilAwaitingInput(self):
        while not (self.terminated() or self.awaitingInput()):
            self.step()
        return not self.error_condition
        
    @staticmethod
    def readPrograms(file_name):
        programs = []
        with open(file_name, newline = '') as f:
            for row in csv.reader(f):
                programs.append(IntCode(row))
        return programs

class BFMazeExplorer:
    def __init__(self, prog):
        self.prog = prog
        self.grid = {}
        self.grid[(0, 0)] = 1
        self.foundOxygen = False
        self.oxygenRoute = []
        self.o2pos = None
        self.x_min = 0
        self.x_max = 0
        self.y_min = 0
        self.y_max = 0
        self.branches = self.expandBranch([])

    def size(self):
        return len(self.branches)

    def xSize(self):
        return 1 + self.x_max - self.x_min

    def ySize(self):
        return 1 + self.y_max - self.y_min

    def getOxygenRoute(self):
        return self.oxygenRoute.copy()

    def mappedSpace(self):
        return len(self.branches) == 0

    def expandBranches(self):
        newBranches = []
        for branch in self.branches:
            newBranch = self.expandBranch(branch)
            for nb in newBranch:
                newBranches.append(nb)
        self.branches = newBranches
        return self.mappedSpace()

    def expandBranch(self, route):
        newBranch = []
        for move in [1, 2, 3, 4]:
            branch = route.copy()
            branch.append(move)
            pos = self.getPos(branch)
            if pos[0] < self.x_min:
                self.x_min = pos[0]
            if pos[0] > self.x_max:
                self.x_max = pos[0]
            if pos[1] < self.y_min:
                self.y_min = pos[1]
            if pos[1] > self.y_max:
                self.y_max = pos[1]
            if not pos in self.grid:
                result = self.moveBot(branch)
                self.grid[pos] = result
                if result == 0:
                    self.moveBack(route)
                else:
                    newBranch.append(branch)
                    if result == 2 and not self.foundOxygen:
                        self.foundOxygen = True
                        self.o2pos = pos
                        self.oxygenRoute.append(branch.copy())
                    self.moveBack(branch)
        return newBranch

    def moveBot(self, route):
        self.prog.reset(route)
        output = -1
        for i in range(len(route)):
            if self.prog.runUntilOutputWritten():
                output = self.prog.getLastOutput()
                if i < len(route) - 1 and output != 1:
                    if output == 0:
                        print("Route", route, "encounters block at point",
                                  i + 1)
        return output

    def moveBack(self, route):
        pass

    def nbrs(self, pos):
        nbr = []
        north = (pos[0], pos[1] + 1)
        south = (pos[0], pos[1] - 1)
        west = (pos[0] - 1, pos[1])
        east = (pos[0] + 1, pos[1])
        for cell in [north, south, west, east]:
            if cell in self.grid and self.grid[cell] != 0:
                nbr.append(cell)
        return nbr

    def diffuseO2(self):
        n = 0
        newGrid = {}
        for pos in self.grid:
            if self.grid[pos] == 1:
                nbrO2 = False
                for nbr in self.nbrs(pos):
                    if self.grid[nbr] == 2:
                        nbrO2 = True
                        break
                if nbrO2:
                    newGrid[pos] = 2
                    n += 1
                else:
                    newGrid[pos] = 1
            else:
                newGrid[pos] = self.grid[pos]
        self.grid = newGrid
        return n

    def getPos(self, route, start = (0, 0)):
        pos = start
        for move in route:
            if move == 1:
                pos = (pos[0], pos[1] + 1)
            elif move == 2:
                pos = (pos[0], pos[1] - 1)
            elif move == 3:
                pos = (pos[0] - 1, pos[1])
            elif move == 4:
                pos = (pos[0] + 1, pos[1])
        return pos

    def printGrid(self, f = sys.stdout):
        for y in range(self.y_min, self.y_max + 1):
            for x in range(self.x_min, self.x_max + 1):
                if (x, y) in self.grid:
                    if self.grid[(x, y)] == 0:
                        f.write("#")
                    elif self.grid[(x, y)] == 1:
                        f.write(".")
                    elif self.grid[(x, y)] == 2:
                        f.write("O")
                    else:
                        f.write(str(self.grid[(x, y)]))
                else:
                    f.write(" ")
            f.write("\n")
        f.write("\n")

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.csv"
    programs = IntCode.readPrograms(input_file)
    print("Read", len(programs), "programs from", input_file)

    maze = BFMazeExplorer(programs[0])
    level = 0
    t0 = time.time_ns()
    while not maze.expandBranches():
        t1 = time.time_ns()
        level += 1
        print("Level", level, "-- size", maze.size(), "-- time (ns)", t1 - t0)
        t0 = time.time_ns()

    print("Rectangle containing search area", maze.xSize(), "cells wide by",
              maze.ySize(), "cells tall")

    o2routes = maze.getOxygenRoute()
    print("Found", len(o2routes), "routes to oxygen")
    for i in range(len(o2routes)):
        print("\tLength of route", i + 1, "is", len(o2routes[i]))
    
    ticks = 0
    while maze.diffuseO2() > 0:
        ticks += 1

    print("Diffused oxygen to all cells in", ticks, "minutes")
        
    sys.exit(0)

if __name__ == "__main__":
    main(sys.argv)
