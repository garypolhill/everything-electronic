#!/usr/bin/env python3

import sys

class Reaction:

    chemicals = {}
    
    def __init__(self, eqstr):
        io = eqstr.split(" => ")
        lhs = io[0].split(", ")
        rhs = io[1]

        z = Reaction.amounts(rhs)
        self.product = {}
        self.product[z[1]] = z[0]

        Reaction.addChemical(z[1], self, rhs = True)

        self.ingredients = {}

        for amt in lhs:
            z = Reaction.amounts(amt)
            self.ingredients[z[1]] = z[0]
            Reaction.addChemical(z[1], self, lhs = True)

    def produces(self, chemical):
        if chemical in self.product:
            return self.product[chemical]
        else:
            return 0

    def needs(self, chemical):
        if chemical in self.ingredients:
            return self.ingredients[chemical]
        else:
            return 0

    def nIngredients(self):
        return len(self.ingredients)

    def ingredientsList(self):
        ingred = []
        for i in self.ingredients:
            ingred.append(i)
        return ingred        

    def reactionsProducingIngredients(self):
        ingred = {}
        for i in self.ingredients:
            ingred[i] = []

            for j in Reaction.chemicals[i]["rhs"]:
                ingred[i].append(j)

        return ingred

    def oreNeeded(self, chemical = "ORE", leftovers = {}):
        n = self.needs(chemical)
        if n > 0:
            return n
        # n must be zero here
        react = self.reactionsProducingIngredients()

        for i in self.ingredients:
            needed = self.ingredients[i]

            reacti = react[i]
            if len(reacti) == 0:
                print("Nothing produces ingredient", i)
                sys.exit(1)
            elif len(reacti) > 1:
                print("More than one reaction produces ingredient", i)
                sys.exit(1)

            react0 = reacti[0]
            prod = react0.produces(i)
            if prod == 0:
                print("PANIC!")
                sys.exit(1)

            n_run = needed // prod
            if needed % prod > 0:
                n_run += 1

            leftover = (prod * n_run) - needed

            n += react0.oreNeeded(chemical, leftovers) * n_run
            if leftover > 0:
                if i in leftovers:
                    leftovers[i] += leftover
                else:
                    leftovers[i] = leftover

        return n
            
    def bfOreNeeded(self, chemical = 'ORE'):
        n = self.needs(chemical)
        if n > 0:
            return n
        else:
            myNeeds = {}
            for i in self.ingredients:
                myNeeds[i] = self.ingredients[i]
            amount = Reaction.bfOreNeededLayer(myNeeds, {}, chemical)
            return amount
            

    @staticmethod
    def bfOreNeededLayer(needsToMeet, leftovers, chemical):
        if len(needsToMeet) == 1 and chemical in needsToMeet:
            for i in leftovers:
                print("\tLeftover", i, "is", leftovers[i])
            return needsToMeet[chemical]

        newNeeds = {}
        newLeftovers = {}
        for i in needsToMeet:
            if i != chemical:
                needed = needsToMeet[i]
                if i in leftovers:
                    if leftovers[i] > needed:
                        newLeftovers[i] = leftovers[i] - needed
                        continue
                    else:
                        needed -= leftovers[i]
                producers = Reaction.chemicals[i]["rhs"]
                producer = producers[0]
                amount = producer.produces(i)
                n_run = needed // amount
                if needsToMeet[i] % amount > 0:
                    n_run += 1
                leftover = (amount * n_run) - needsToMeet[i]
                if leftover > 0:
                    if i in newLeftovers:
                        newLeftovers[i] += leftover
                    else:
                        newLeftovers[i] = leftover
                
                for j in producer.ingredientsList():
                    if j in newNeeds:
                        newNeeds[j] += n_run * producer.needs(j)
                    else:
                        newNeeds[j] = n_run * producer.needs(j)
            else:
                newNeeds[i] = needsToMeet[i]

        for i in leftovers:
            leftover = leftovers[i]
            if i in newLeftovers:
                leftover = newLeftovers[i]
                del newLeftovers[i]       # We will put it back later...
                
            if i in newNeeds:
                if leftover > newNeeds[i]:
                    leftover -= newNeeds[i]
                    del newNeeds[i]
                else:
                    newNeeds[i] -= leftover
                    leftover = 0
            if leftover > 0:
                newLeftovers[i] = leftover   # ...here in fact
                
        return Reaction.bfOreNeededLayer(newNeeds, newLeftovers, chemical)

    @staticmethod
    def checkAssumptions():
        return Reaction.checkOreAlone() and Reaction.checkChemicalsOnce()

    @staticmethod
    def checkOreAlone():
        for react in Reaction.chemicals["ORE"]["lhs"]:
            if react.nIngredients() > 1:
                return False
        return True

    @staticmethod
    def checkChemicalsOnce():
        for chem in Reaction.chemicals:
            if len(Reaction.chemicals[chem]["rhs"]) > 1:
                return False
        return True
            
    @staticmethod
    def amounts(str):
        arr = str.split(" ")
        return (int(arr[0]), arr[1])

    @staticmethod
    def addChemical(name, reaction, lhs = False, rhs = False):
        if name not in Reaction.chemicals:
            Reaction.chemicals[name] = {}
            Reaction.chemicals[name]["lhs"] = []
            Reaction.chemicals[name]["rhs"] = []

        if lhs and not reaction in Reaction.chemicals[name]["lhs"]:
            Reaction.chemicals[name]["lhs"].append(reaction)

        if rhs and not reaction in Reaction.chemicals[name]["rhs"]:
            Reaction.chemicals[name]["rhs"].append(reaction)

def test(id, arr, ore_t):
    Reaction.chemicals = {}
    reactions = []
    for string in arr:
        reactions.append(Reaction(string))

    if Reaction.checkAssumptions():
        print("Assumptions valid")
    else:
        print("Assumptions not valid")
        if not Reaction.checkOreAlone():
            print("There are reactions in which ORE is not on its own on LHS")
        if not Reaction.checkChemicalsOnce():
            print("There are chemicals with more than one reaction making them")
        sys.exit(1)
        
    fuel = Reaction.chemicals['FUEL']["rhs"]
    leftovers = {}
    ore = fuel[0].oreNeeded('ORE', leftovers)
    if(ore == ore_t):
        print("Test", id, "-- passed")
    else:
        print("Test", id, "-- FAILED:", ore, "ore calculated;", ore_t,
                  "ore expected")
    for chemical in leftovers:
        print("\tLeftover", chemical, "=", leftovers[chemical])

def bftest(id, arr, ore_t):
    print("Breadth first test", id)
    Reaction.chemicals = {}
    reactions = []
    for string in arr:
        reactions.append(Reaction(string))

    fuel = Reaction.chemicals['FUEL']["rhs"]
    ore = fuel[0].bfOreNeeded()
    if(ore == ore_t):
        print("\t-- passed")
    else:
        print("\t-- FAILED:", ore, "ore calculated;", ore_t, "ore expected")    

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    test1 = ["10 ORE => 10 A",
             "1 ORE => 1 B",
             "7 A, 1 B => 1 C",
             "7 A, 1 C => 1 D",
             "7 A, 1 D => 1 E",
             "7 A, 1 E => 1 FUEL"]
    test(1, test1, 31)
    bftest(1, test1, 31)

    test2 = ["9 ORE => 2 A",
             "8 ORE => 3 B",
             "7 ORE => 5 C",
             "3 A, 4 B => 1 AB",
             "5 B, 7 C => 1 BC",
             "4 C, 1 A => 1 CA",
             "2 AB, 3 BC, 4 CA => 1 FUEL"]
    test(2, test2, 165)
    bftest(2, test2, 165)

    test3 = ["157 ORE => 5 NZVS",
             "165 ORE => 6 DCFZ",
             "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL",
             "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ",
             "179 ORE => 7 PSHF",
             "177 ORE => 5 HKGWZ",
             "7 DCFZ, 7 PSHF => 2 XJWVT",
             "165 ORE => 2 GPVTF",
             "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT"]
    test(3, test3, 13312)
    bftest(3, test3, 13312)

    test4 = ["2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG",
        "17 NVRVD, 3 JNWZP => 8 VPVL",
        "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL",
        "22 VJHF, 37 MNCFX => 5 FWMGM",
        "139 ORE => 4 NVRVD",
        "144 ORE => 7 JNWZP",
        "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC",
        "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV",
        "145 ORE => 6 MNCFX",
        "1 NVRVD => 8 CXFTF",
        "1 VJHF, 6 MNCFX => 4 RFSQX",
        "176 ORE => 6 VJHF"]
    test(4, test4, 180697)
    bftest(4, test4, 180697)

    test5 = ["171 ORE => 8 CNZTR",
        "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL",
        "114 ORE => 4 BHXH",
        "14 VRPVC => 6 BMBT",
        "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL",
        "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT",
        "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW",
        "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW",
        "5 BMBT => 4 WPTQ",
        "189 ORE => 9 KTJDG",
        "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP",
        "12 VRPVC, 27 CNZTR => 2 XDBXC",
        "15 KTJDG, 12 BHXH => 5 XCVML",
        "3 BHXH, 2 VRPVC => 7 MZWV",
        "121 ORE => 7 VRPVC",
        "7 XCVML => 6 RJRHP",
        "5 BHXH, 4 VRPVC => 5 LTCX"]
    test(5, test5, 2210736)
    bftest(5, test5, 2210736)
    
    Reaction.chemicals = {}
    reactions = []
    with open(input_file) as f:
        for line in f:
            if "=>" in line:
                reactions.append(Reaction(line[:-1]))

    if Reaction.checkAssumptions():
        print("Assumptions valid")
    else:
        print("Assumptions not valid")
        if not Reaction.checkOreAlone():
            print("There are reactions in which ORE is not on its own on LHS")
        if not Reaction.checkChemicalsOnce():
            print("There are chemicals with more than one reaction making them")
        sys.exit(1)
    fuel = Reaction.chemicals['FUEL']["rhs"]
    ore = fuel[0].oreNeeded()

    print("Amount of ore needed:", ore)

if __name__ == "__main__":
    main(sys.argv)
