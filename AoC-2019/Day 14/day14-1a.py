#!/usr/bin/env python3

import sys
import numpy as np

class System:

    def __init__(self, eqarr):
        self.names = {}                   # Chemical name to index
        self.n = 0                        # Next index
        mat = {}

        for eqstr in eqarr:
            io = eqstr.split(" => ")
            lhs = io[0].split(", ")
            rhs = io[1]

            r = System.amounts(rhs)
            mat[r[1], r[1]] = r[0]

            if not r[1] in self.names:
                self.names[r[1]] = self.n
                self.n += 1

            for lhsi in lhs:
                l = System.amounts(lhsi)
                mat[l[1], r[1]] = l[0]
                if not l[1] in self.names:
                    self.names[l[1]] = self.n
                    self.n += 1

        self.system = np.zeros((self.n, self.n), dtype = int)
        for (i, j) in mat:
            self.system[self.names[i], self.names[j]] = mat[i, j]

    def oreNeededForFuel(self, ore = "ORE", fuel = "FUEL"):
        ore_i = self.names[ore]
        fuel_i = self.names[fuel]
        chem = self.system[:, fuel_i].copy()
        amount = chem[fuel_i]
        chem[fuel_i] = 0
        left = np.zeros(self.n, dtype = int)
        left[fuel_i] = amount
        while not self.substituteOre(chem, left, ore):
            print("chem:", chem)
            print("left:", left)
        return chem[ore_i]

    def substituteOre(self, chem, left, ore = "ORE"):
        ore_i = self.names[ore]
        nz = chem.nonzero()
        if len(nz[0]) == 1 and nz[0][0] == ore_i:
            return True
        else:
            for i in nz[0]:
                if i == ore_i:
                    continue
                need_i = chem[i]
                make_i = self.system[:, i].copy()
                prod_i = make_i[i]

                rpt_i = need_i // prod_i
                if need_i % prod_i > 0:
                    rpt_i += 1

                left[i] += (prod_i * rpt_i) - need_i

                make_i *= rpt_i
                chem += make_i
                chem[i] = 0
            return False

    def oreDirect(self, ore = "ORE"):
        ore_i = self.names[ore]
        ore_chem = self.system[ore_i, :].copy()
        ocnz = ore_chem.nonzero()
        return ocnz[0]
            
    @staticmethod
    def amounts(st):
        arr = st.split(" ")
        return (int(arr[0]), arr[1])


def test(ID, sysstr, exp, mthd = "substitute"):
    system = System(sysstr)
    ore = "NA"
    if mthd == "substitute":
        ore = system.oreNeededForFuel()
    else:
        print("No method", mthd)
        sys.exit(1)

    if ore == exp:
        print("Method", mthd, "Test", ID, "-- passed")
    else:
        print("Method", mthd, "Test", ID, "-- FAILED:", ore,
                  "ore calculated;", exp, "ore expected")
        
    

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.txt"

    test1 = ["10 ORE => 10 A",
             "1 ORE => 1 B",
             "7 A, 1 B => 1 C",
             "7 A, 1 C => 1 D",
             "7 A, 1 D => 1 E",
             "7 A, 1 E => 1 FUEL"]
    test(1, test1, 31)
    test2 = ["9 ORE => 2 A",
             "8 ORE => 3 B",
             "7 ORE => 5 C",
             "3 A, 4 B => 1 AB",
             "5 B, 7 C => 1 BC",
             "4 C, 1 A => 1 CA",
             "2 AB, 3 BC, 4 CA => 1 FUEL"]
    test(2, test2, 165)
    test3 = ["157 ORE => 5 NZVS",
             "165 ORE => 6 DCFZ",
             "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL",
             "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ",
             "179 ORE => 7 PSHF",
             "177 ORE => 5 HKGWZ",
             "7 DCFZ, 7 PSHF => 2 XJWVT",
             "165 ORE => 2 GPVTF",
             "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT"]
    test(3, test3, 13312)
    test4 = ["2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG",
        "17 NVRVD, 3 JNWZP => 8 VPVL",
        "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL",
        "22 VJHF, 37 MNCFX => 5 FWMGM",
        "139 ORE => 4 NVRVD",
        "144 ORE => 7 JNWZP",
        "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC",
        "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV",
        "145 ORE => 6 MNCFX",
        "1 NVRVD => 8 CXFTF",
        "1 VJHF, 6 MNCFX => 4 RFSQX",
        "176 ORE => 6 VJHF"]
    test(4, test4, 180697)
    test5 = ["171 ORE => 8 CNZTR",
        "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL",
        "114 ORE => 4 BHXH",
        "14 VRPVC => 6 BMBT",
        "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL",
        "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT",
        "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW",
        "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW",
        "5 BMBT => 4 WPTQ",
        "189 ORE => 9 KTJDG",
        "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP",
        "12 VRPVC, 27 CNZTR => 2 XDBXC",
        "15 KTJDG, 12 BHXH => 5 XCVML",
        "3 BHXH, 2 VRPVC => 7 MZWV",
        "121 ORE => 7 VRPVC",
        "7 XCVML => 6 RJRHP",
        "5 BHXH, 4 VRPVC => 5 LTCX"]
    test(5, test5, 2210736)

    reactions = []
    with open(input_file) as f:
        for line in f:
            if "=>" in line:
                reactions.append(line[:-1])
    system = System(reactions)
    ore = system.oreNeededForFuel()
    print("Amount of ore needed:", ore)

if __name__ == "__main__":
    main(sys.argv)
