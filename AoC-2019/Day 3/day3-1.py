#!/usr/bin/env python3

import csv, sys

class Wire:

    def __init__(self, route):
        self.points = Wire.routeToPoints(route)

    def hasPoint(self, point):
        return point in self.points

    def intersections(self, wire):
        point_list = []
        
        for point in self.points.keys():
            if wire.hasPoint(point):
                point_list.append(point)

        return point_list

    @staticmethod
    def routeToPoints(route):
        points = {}
        x = 0
        y = 0
        i = 0
        for vector in route:
            direction = vector[0]
            length = int(vector[1:])
            
            for step in range(length):
                i += 1
                
                if direction == "U":
                    y += 1
                elif direction == "D":
                    y -= 1
                elif direction == "L":
                    x -= 1
                elif direction == "R":
                    x += 1
                else:
                    print("Invalid direction", direction)
                    sys.exit(1)
                    
                if (x, y) in points:
                    points[x, y].append(i)
                else:
                    points[x, y] = [i]
                    
        return points

    @staticmethod
    def readWires(file_name):
        wires = []
        with open(file_name, newline = '') as f:
            for row in csv.reader(f):
                wires.append(Wire(row))
        return wires

def nearestIntersection(wire1, wire2):
    ndist = -1
    xover = wire1.intersections(wire2)
    for point in xover:
        dist = abs(point[0]) + abs(point[1])
        if ndist == -1 or dist < ndist:
            ndist = dist
    return ndist

def test(id, wire1, wire2, ndist):
    calcndist = nearestIntersection(wire1, wire2)
    if(calcndist == ndist):
        print("Test", id, "-- pass")
        return True
    else:
        print("Test", id, "-- FAIL: calculated", calcndist, "expected", ndist)
        return False

def main(argv):
    input_file = argv[1] if len(argv) > 1 else "input.csv"
    wires = Wire.readWires(input_file)

    # Tests
    ok = test(1, Wire(["R75", "D30", "R83", "U83", "L12",
                           "D49", "R71", "U7", "L72"]),
                  Wire(["U62", "R66", "U55", "R34", "D71",
                            "R55", "D58", "R83"]), 159)
    ok = test(2, Wire(["R98", "U47", "R26", "D63", "R33", "U87",
                      "L62", "D20", "R33", "U53", "R51"]),
                Wire(["U98", "R91", "D20", "R16", "D67", "R40",
                     "U7", "R15", "U6", "R7"]), 135) and ok

    # Puzzle answer
    ndist = nearestIntersection(wires[0], wires[1])
    print("Answer:", ndist)
    sys.exit(0)

if __name__ == "__main__":
    main(sys.argv)
