from microbit import *

threshold_level = 1
while True:
    if button_a.is_pressed():
        threshold_level = threshold_level + 1
    elif button_b.is_pressed():
        threshold_level = threshold_level - 1
    if threshold_level < 0:
        threshold_level = 0
        display.show("-")
        sleep(1000)
    if threshold_level > 255:
        threshold_level = 255
        display.show("+")
        sleep(1000)
    light_level = display.read_light_level()
    if light_level > threshold_level:
        display.show(Image.XMAS)
    else:
        display.show(Image.HAPPY)